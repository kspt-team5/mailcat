package kspt.team5.mailcat.web.routing;

import com.google.common.collect.Lists;
import kspt.team5.mailcat.core.boundaries.CategoryViewer;
import kspt.team5.mailcat.core.boundaries.MailViewer;
import kspt.team5.mailcat.core.boundaries.SettingsViewer;
import kspt.team5.mailcat.core.entities.Category;
import kspt.team5.mailcat.core.entities.Email;
import kspt.team5.mailcat.core.interactors.ApplicationController;
import kspt.team5.mailcat.core.interactors.ViewController;
import kspt.team5.mailcat.web.CoreComponent;
import kspt.team5.mailcat.web.entities.CategorizedEmail;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class WebControllerTest {
    @LocalServerPort
    private int port;

    @Mock
    private ViewController mockedViewController;

    @MockBean
    private ApplicationController applicationController;

    @Autowired
    private CoreComponent core;

    @Autowired
    private TestRestTemplate restTemplate;

    @Before
    public void setUp() {
        when(applicationController.getViewController()).thenReturn(mockedViewController);
    }

    @Test
    public void settingsTest() {
        // given
        final Map<String, Object> expectedSettings = getExpectedSettings();
        mockApplicationControllerSettingsWith(expectedSettings);
        // when
        final ResponseEntity<Map<String, Object>> responseEntity =
                restTemplate.exchange("http://localhost:" + port + "/api/settings", HttpMethod.GET, null,
                        new ParameterizedTypeReference<Map<String, Object>>() {});
        final Map<String, Object> settings = responseEntity.getBody();
        // then
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(settings).isEqualTo(expectedSettings);
    }

    private void mockApplicationControllerSettingsWith(Map<String, Object> expectedSettings) {
        SettingsViewer mockedSettingsViewer = mock(SettingsViewer.class);
        when(mockedSettingsViewer.getSettings()).thenReturn(expectedSettings);
        when(mockedViewController.getSettingsViewer()).thenReturn(mockedSettingsViewer);
    }

    private static Map<String, Object> getExpectedSettings() {
        final Map<String, Object> result = new LinkedHashMap<>();
        result.put("email", "email@example.com");
        result.put("password", "pass");
        result.put("host", "imap.example.com");
        result.put("port", 993);
        return result;
    }

    @Test
    public void categoriesTest() {
        //given
        final List<String> expectedCategories = Lists.newArrayList("Category1", "Category2");
        mockApplicationControllerCategoriesWith(expectedCategories);
        //when
        final ResponseEntity<List<String>> responseEntity =
                restTemplate.exchange("http://localhost:" + port + "/api/categories", HttpMethod.GET, null,
                        new ParameterizedTypeReference<List<String>>() {});
        final List<String> categories = responseEntity.getBody();
        //then
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(categories).containsOnlyElementsOf(expectedCategories);
    }

    private void mockApplicationControllerCategoriesWith(List<String> expectedCategories) {
        CategoryViewer mockedCategoryViewer = mock(CategoryViewer.class);
        when(mockedCategoryViewer.getCategories()).thenReturn(asCategorySet(expectedCategories));
        when(mockedViewController.getCategoryViewer()).thenReturn(mockedCategoryViewer);
    }

    private Set<Category> asCategorySet(List<String> categories) {
        Set<Category> setOfCategories = new HashSet<>();
        for (String category : categories) {
            setOfCategories.add(new Category(category));
        }
        return setOfCategories;
    }

    @Test
    public void categorizedEmailsTest() {
        //given
        final List<CategorizedEmail> expectedCategorizedEmails = getExpectedCategorizedEmails();
        mockApplicationControllerCategorizedEmailsWith(expectedCategorizedEmails);
        //when
        final ResponseEntity<List<CategorizedEmail>> responseEntity =
                restTemplate.exchange("http://localhost:" + port + "/api/last", HttpMethod.GET, null,
                        new ParameterizedTypeReference<List<CategorizedEmail>>() {});
        final List<CategorizedEmail> categorizedEmails = responseEntity.getBody();
        //then
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(categorizedEmails).containsOnlyElementsOf(expectedCategorizedEmails);
    }

    private void mockApplicationControllerCategorizedEmailsWith(List<CategorizedEmail> expectedCategorizedEmails) {
        MailViewer mockedCategorizedEmailViewer = mock(MailViewer.class);
        when(mockedCategorizedEmailViewer.getLastCategorizedEmails())
                .thenReturn(asEmailToCategoryMap(expectedCategorizedEmails));
        when(mockedViewController.getMailViewer()).thenReturn(mockedCategorizedEmailViewer);
    }


    private Map<Email, Category> asEmailToCategoryMap(List<CategorizedEmail> expectedCategorizedEmails) {
        Map<Email, Category> map = new HashMap<>();
        for (int i = 0; i < expectedCategorizedEmails.size(); i++) {
            final CategorizedEmail categorizedEmail = expectedCategorizedEmails.get(i);
            final Email email = new Email(String.valueOf(i), categorizedEmail.getSubject(),
                    "ContentType" + i, "ContentType" + i, "@" + i, categorizedEmail.getSender());
            final Category category = new Category(categorizedEmail.getCategory());
            map.put(email, category);
        }
        return map;
    }

    private List<CategorizedEmail> getExpectedCategorizedEmails() {
        return Lists.newArrayList(
                new CategorizedEmail("@1", "Subject1", "", "Category1"),
                new CategorizedEmail("@2", "Subject2", "", "Category2"));
    }
}