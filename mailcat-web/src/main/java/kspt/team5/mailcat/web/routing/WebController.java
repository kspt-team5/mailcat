package kspt.team5.mailcat.web.routing;

import com.google.common.collect.Lists;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import kspt.team5.mailcat.core.delivery.IMAP4MailService;
import kspt.team5.mailcat.core.entities.Category;
import kspt.team5.mailcat.core.entities.Config;
import kspt.team5.mailcat.core.entities.Email;
import kspt.team5.mailcat.core.entities.Folder;
import kspt.team5.mailcat.web.CoreComponent;
import kspt.team5.mailcat.web.config.SpringConfig;
import kspt.team5.mailcat.web.entities.CategorizedEmail;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Api
@CrossOrigin
@RestController
public class WebController {
    @Autowired
    private CoreComponent core;

    @GetMapping("/")
    String home() {
        return "MailCat Web - On\nMailCat Core - " + (core.getApplicationController().isRun() ? "On" : "Off");
    }

    @PostMapping(value = "/api/init", consumes = "application/json")
    @ApiOperation("Initialize and run MailCat Core")
    void init(@RequestBody Config config) {
        core.run(config);
    }

    @GetMapping(value = "/api/settings", produces = "application/json")
    @ApiOperation("Get application settings")
    Map<String, Object> settings() {
        return core.getApplicationController().getViewController().getSettingsViewer().getSettings();
    }

    @GetMapping(value = "/api/categories", produces = "application/json")
    @ApiOperation("Get list of email categories")
    List<String> categories() {
        Set<Category> categories =
                core.getApplicationController().getViewController().getCategoryViewer().getCategories();
        return categories.stream().map(Category::getName).collect(Collectors.toList());
    }

    @GetMapping(value = "/api/last", produces = "application/json")
    @ApiOperation("Get list of last categorized emails")
    List<CategorizedEmail> categorizedEmails() {
        Map<Email, Category> emails =
                core.getApplicationController().getViewController().getMailViewer().getLastCategorizedEmails();
        return asCategorizedEmailsList(emails);
    }

    private static List<CategorizedEmail> asCategorizedEmailsList(Map<Email, Category> emails) {
        List<CategorizedEmail> list = Lists.newArrayList();
        for (Map.Entry<Email, Category> entry : emails.entrySet()) {
            Email e = entry.getKey();
            list.add(new CategorizedEmail(
                    e.getSender(), e.getSubject(), "", entry.getValue().getName()));
        }
        return list;
    }

    @RequestMapping(value = "/api/folders", produces = "application/json", method = RequestMethod.POST)
    @ApiOperation("Get list of folders")
    @ResponseBody
    List<Folder> getFolderList(@RequestBody ConfigData config) {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(SpringConfig.class);
        List<Folder> list = Lists.newArrayList();
        IMAP4MailService mailService = ctx.getBean(IMAP4MailService.class);
        mailService.connect(config.host, config.port, config.email, config.password);
        Set<Folder> setOfFolders = mailService.getFolders();
        setOfFolders.forEach(f -> {
            if (!f.getName().contains("[Gmail]") && !f.getName().equalsIgnoreCase("INBOX"))
                list.add(f);
        });
        mailService.disconnect();
        return list;
    }
    @AllArgsConstructor
    static class ConfigData {
        String email;
        String password;
        int learningThreshold = 50;
        String host;
        int port;

    }
}