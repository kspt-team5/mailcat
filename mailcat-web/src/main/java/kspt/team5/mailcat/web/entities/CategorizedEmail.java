package kspt.team5.mailcat.web.entities;

import lombok.Value;

import java.time.LocalDateTime;

@Value
public class CategorizedEmail {
    String sender;

    String subject;

    String dateTime;

    String category;
}
