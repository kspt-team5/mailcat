package kspt.team5.mailcat.web.config;

import kspt.team5.mailcat.core.boundaries.KnowledgeStorage;
import kspt.team5.mailcat.core.classification.AverageBasedClassifier;
import kspt.team5.mailcat.core.classification.ClassificationCouncil;
import kspt.team5.mailcat.core.classification.MailClassifier;
import kspt.team5.mailcat.core.classification.SenderClassifier;
import kspt.team5.mailcat.core.delivery.FileKnowledgeStorage;
import kspt.team5.mailcat.core.delivery.IMAP4MailService;
import kspt.team5.mailcat.core.interactors.ApplicationController;
import kspt.team5.mailcat.core.utilities.ApplicationControllerProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.AsyncListenableTaskExecutor;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;

import java.util.Set;

@EnableAsync
@Configuration
public class SpringConfig {
    @Bean
    AsyncListenableTaskExecutor getTaskExecutor() {
        return new SimpleAsyncTaskExecutor();
    }

    @Bean
    ApplicationController getApplicationController() {
        return ApplicationControllerProvider.builder()
                .mailClassifier(getMailClassifier())
                .knowledgeStorage(getKnowledgeStorage())
                .build().get();
    }

    @Bean
    IMAP4MailService mailService() {
        return new IMAP4MailService();
    }


    private MailClassifier getMailClassifier() {
        final ClassificationCouncil council =
                (ClassificationCouncil) getKnowledgeStorage().load(ClassificationCouncil.class);
        if (council.getMembers().isEmpty()) {
            council.registerMembers(
                    new SenderClassifier()//,
                    //new ImageCountClassifier(),
                    //new HrefCountClassifier(),
                    //new HtmlTagsCountClassifier(),
                    //new WBMailClassifier()
            );
        }
        AverageBasedClassifier.USE_CONFIDENCE_INTERVALS = true;
        return council;
    }

    private KnowledgeStorage getKnowledgeStorage() {
        return new FileKnowledgeStorage();
    }
}
