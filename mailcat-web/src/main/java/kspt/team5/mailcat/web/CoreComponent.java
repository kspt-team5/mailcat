package kspt.team5.mailcat.web;

import kspt.team5.mailcat.core.entities.Config;
import kspt.team5.mailcat.core.interactors.ApplicationController;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.AsyncListenableTaskExecutor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;

import javax.annotation.PreDestroy;

@Slf4j
@Component
public class CoreComponent {
    @Autowired
    private AsyncListenableTaskExecutor executor;

    @Getter
    private final ApplicationController applicationController;

    @Autowired
    public CoreComponent(ApplicationController applicationController) {
        this.applicationController = applicationController;
    }

    @Async
    public void run(final Config config) {
        ListenableFuture<?> future = executor.submitListenable(() -> applicationController.run(config));
        future.addCallback((o) -> log.debug("Execution of mailcat-core completed successfully"),
                (throwable) -> {
                    log.error("Execution of mailcat-core stopped because of an error:", throwable);
                    try {
                        log.warn("Waiting for {} seconds to restart...", 3);
                        Thread.sleep(3000);
                        this.run(config);
                        log.warn("Restarted mailcat-core.");
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                });
    }

    @PreDestroy
    public void stop() {
        applicationController.stop();
    }
}
