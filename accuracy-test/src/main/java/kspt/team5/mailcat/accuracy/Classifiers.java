package kspt.team5.mailcat.accuracy;

public enum Classifiers {
    WB,
    Image,
    Href,
    Html,
    Sender
}
