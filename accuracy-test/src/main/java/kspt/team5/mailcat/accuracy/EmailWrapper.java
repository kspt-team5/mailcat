package kspt.team5.mailcat.accuracy;

import kspt.team5.mailcat.core.entities.Category;
import kspt.team5.mailcat.core.entities.Email;
import lombok.Value;

@Value
class EmailWrapper {
    Email email;
    Category category;
}

