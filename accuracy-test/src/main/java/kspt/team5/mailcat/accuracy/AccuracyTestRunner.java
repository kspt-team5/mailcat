package kspt.team5.mailcat.accuracy;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.cli.*;

import java.nio.file.Paths;

@Slf4j
public class AccuracyTestRunner {

    private static final Options options = new Options();
    private static final Option sorted = new Option("s", "sorted-dir", true,
            "Sorted teaching sample path");
    private static final Option inbox = new Option("i", "inbox-dir", true,
            "Sorted testing sample path (for bunch or watch test)");
    private static final Option type = new Option("t", "type", true,
            "Type of test (classic, bunch or watch)");
    private static final Option classifiers = new Option("c", "classifiers", true,
            "List of classifiers to register separated by comma (WB, Image, Href, Html and Sender)");

    public static void main(String[] args) {
        setOptions();
        CommandLine cmd = parseOptionsFromArgs(args);
        AccuracyTest accuracyTest = new AccuracyTest();
        if (cmd.hasOption("classifiers"))
            accuracyTest.setClassifiers(cmd.getOptionValues("classifiers"));
        accuracyTest.setSortedPath(Paths.get(cmd.getOptionValue("sorted-dir")));
        runTest(cmd, accuracyTest);
    }

    private static void setOptions() {
        setOptionSorted();
        setOptionInbox();
        setOptionType();
        setOptionClassifiers();
    }

    private static CommandLine parseOptionsFromArgs(String[] args) {
        CommandLine cmd;
        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        try {
            cmd = parser.parse(options, args);
            return cmd;
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("mailcat", options);
            System.exit(1);
            return null;
        }
    }

    private static void runTest(CommandLine cmd, AccuracyTest accuracyTest) {
        if (cmd.getOptionValue("type").equals("bunch"))
            runTestBunch(cmd, accuracyTest);
        else if (cmd.getOptionValue("type").equals("watch"))
            runTestWatch(cmd, accuracyTest);
        else if (cmd.getOptionValue("type").equals("classic")) {
            accuracyTest.testAccuracy_classic();
        } else {
            System.out.println("invalid test type " + cmd.getOptionValue("type"));
            System.exit(1);
        }
    }

    private static void setOptionSorted() {
        sorted.setRequired(true);
        options.addOption(sorted);
    }

    private static void setOptionInbox() {
        inbox.setRequired(false);
        options.addOption(inbox);
    }

    private static void setOptionType() {
        type.setRequired(true);
        options.addOption(type);
    }

    private static void setOptionClassifiers() {
        classifiers.setRequired(false);
        classifiers.setValueSeparator(',');
        classifiers.setArgs(5);
        options.addOption(classifiers);
    }

    private static void runTestBunch(CommandLine cmd, AccuracyTest accuracyTest) {
        if (cmd.hasOption("inbox-dir")) {
            accuracyTest.setInboxPath(Paths.get(cmd.getOptionValue("inbox-dir")));
            accuracyTest.testAccuracy_bunch();
        } else {
            System.out.println("Parameter inbox-dir must be set for \"bunch\" test type");
            System.exit(1);
        }
    }

    private static void runTestWatch(CommandLine cmd, AccuracyTest accuracyTest) {
        if (cmd.hasOption("inbox-dir")){
            accuracyTest.setInboxPath(Paths.get(cmd.getOptionValue("inbox-dir")));
            accuracyTest.testAccuracy_watch();
        } else {
            System.out.println("Parameter inbox-dir must be set for \"watch\" test type");
            System.exit(1);
        }
    }
}

