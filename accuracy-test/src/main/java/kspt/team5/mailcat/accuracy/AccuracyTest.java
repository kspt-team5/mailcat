package kspt.team5.mailcat.accuracy;

import kspt.team5.mailcat.WBClassifier.actors.WBMailClassifier;
import kspt.team5.mailcat.core.classification.*;
import kspt.team5.mailcat.core.entities.Category;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;

@Slf4j
@RequiredArgsConstructor
public class AccuracyTest {
    /*
     * "Sorted", "Inbox" and "InboxBunch" folders must be in "mailcat/accuracy-test/out" folder
     * in order to be invisible for git.
     *
     * "Sorted" must be filled with folders (categories) with emails (json files)
     * "InboxBunch" must be filled with SAME folders (categories) with ANOTHER emails (json files)
     * "Inbox" -- put here some json-emails, run "Watch" test and watch how they classified.
     *
     * There are three type of tests:
     *
     * "Classic" stands for old accuracy test concept from WBMailClassifier prototype age
     * that takes one email from context of others sorted emails and pretend it need to be classified.
     *
     * "Bunch" stands for accuracy test on a bunch of sorted emails, but test give them to classifier
     * as unclassified (test sampling) and ask the classifier to classify. Classifier classifies
     * using own knowledge base built from "Sorted" folder, considering it is a learning sample.
     *
     * "Watch" stands for just run and watch how the json-emails in "Inbox" folder classified.
     *
     */
    @Setter
    private Path sortedPath;
    @Setter
    private Path inboxPath;
    @Setter
    private String[] classifiers = {""};

    private ArrayList<EmailWrapper> emailsSorted;
    private double success;
    private double uncategorized;
    private double allCountableTries;
    private double allMessages;
    private boolean classic;

    public void testAccuracy_classic() {
        log.info("Classic test");
        emailsSorted = GetTestingResource.loadJsonCategorizedEmails(sortedPath.toString());
        for (EmailWrapper email : emailsSorted)
            performTest_classic(email);
        show_result();
        ClassificationCouncil.CLASS_TO_NULLSCOUNT.forEach((clazz, count) ->
                log.debug("{} was uncertain {} times", clazz.getSimpleName(), count));
    }

    public void testAccuracy_bunch() {
        log.info("Bunch test");
        MailClassifier classifier = buildClassifier();
        emailsSorted = GetTestingResource.loadJsonCategorizedEmails(sortedPath.toString());
        ArrayList<EmailWrapper> incomingEmails = GetTestingResource.loadJsonCategorizedEmails(inboxPath.toString());
        for (EmailWrapper email : emailsSorted)
            classifier.teach(email.getEmail(), email.getCategory());
        for (EmailWrapper email : incomingEmails) {
            Category result = classifier.classify(email.getEmail());
            try {
                if (email.getCategory().equals(result)) {
                    log.debug("+ " + result.getName() + ": " + email.getEmail().getId());
                    success += 1d;
                } else {
                    log.debug("- " + result.getName() + " (" + email.getCategory().getName() + "): " + email.getEmail().getId());
                }
                allCountableTries += 1d;
            } catch (NullPointerException e) {
                log.debug("? " + "Uncategorized" + " (" + email.getCategory().getName() + "): " + email.getEmail().getId());
                uncategorized += 1d;
            }
            allMessages += 1d;
        }
        show_result();
    }

    public void testAccuracy_watch() {
        emailsSorted = GetTestingResource.loadJsonCategorizedEmails(sortedPath.toString());
        log.info("Watch test");
        MailClassifier classifier = buildClassifier();
        ArrayList<EmailWrapper> incomingEmails = GetTestingResource.loadJsonEmails(new File(inboxPath.toString()));
        for (EmailWrapper email : emailsSorted)
            classifier.teach(email.getEmail(), email.getCategory());
        for (EmailWrapper email : incomingEmails) {
            Category result = classifier.classify(email.getEmail());
            try {
                log.info("? " + result.getName() + ": " + email.getEmail().getId());
            } catch (NullPointerException e) {
                log.debug("? " + "Uncategorized" + ": " + email.getEmail().getId());
            }
        }
    }

    private void performTest_classic(EmailWrapper message) {
        Category result = runClassifier(message);
        classic = true;
        try {
            if (message.getCategory().equals(result)) {
                log.debug("+ " + result.getName() + ": " + message.getEmail().getId());
                success += 1d;
            } else {
                log.debug("- " + result.getName() + " (" + message.getCategory().getName() + "): " +
                        message.getEmail().getId());
            }
            allCountableTries += 1d;
        } catch (NullPointerException e) {
            log.debug("? " + "Uncategorized" + " (" + message.getCategory().getName() + "): " +
                    message.getEmail().getId());
            uncategorized += 1d;
        }
        allMessages += 1d;
    }

    private Category runClassifier(EmailWrapper incomingMessage) {
        MailClassifier classifier = buildClassifier();
        for (EmailWrapper email : emailsSorted)
            if (!email.equals(incomingMessage))
                classifier.teach(email.getEmail(), email.getCategory());
        return classifier.classify(incomingMessage.getEmail());
    }

    private MailClassifier buildClassifier() {
        ClassificationCouncil classifier = new ClassificationCouncil();
        if (!classic) log.info("Classifiers:");
        if (classifiers[0].isEmpty() && classifiers.length == 1) {
            if (!classic) log.info("\tNone");
            return classifier;
        }
        for (String value : classifiers) {
            switch (value) {
                case "WB":
                    classifier.registerMember(new WBMailClassifier());
                    if (!classic) log.info("\tWBMailClassifier");
                    break;
                case "Image":
                    classifier.registerMember(new ImageCountClassifier());
                    if (!classic) log.info("\tImageCountClassifier");
                    break;
                case "Href":
                    classifier.registerMember(new HrefCountClassifier());
                    if (!classic) log.info("\tHrefCountClassifier");
                    break;
                case "Html":
                    classifier.registerMember(new HtmlTagsCountClassifier());
                    if (!classic) log.info("\tHtmlTagsCountClassifier");
                    break;
                case "Sender":
                    classifier.registerMember(new SenderClassifier());
                    if (!classic) log.info("\tSenderClassifier");
                    break;
                default:
                    if (!classic) log.info("\tUnrecognized classifier: " + value);
                    break;
            }
        }
        return classifier;
    }

    private void show_result() {
        log.info("Success rate = " + String.format("%.1f",(success / allCountableTries * 100d)) + "%");
        log.info("Uncategorized rate = " + String.format("%.1f",(uncategorized / allMessages * 100d)) + "%");
    }

}

