package kspt.team5.mailcat.accuracy;

import com.fasterxml.jackson.databind.ObjectMapper;
import kspt.team5.mailcat.core.entities.Category;
import kspt.team5.mailcat.core.entities.Email;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

@Slf4j
public class GetTestingResource {

    public static ArrayList<EmailWrapper> loadJsonCategorizedEmails(final String dirName) {
        ArrayList<EmailWrapper> emails = new ArrayList<>();
        File dir = new File(dirName);
        if (dir.exists() && dir.isDirectory()) {
            File[] cats = dir.listFiles();
            for (File cat : cats)
                if (cat.isDirectory())
                    emails.addAll(loadJsonEmails(cat));
        } else {
            System.out.println("No such directory: \"" + dirName + "\"");
        }
        return emails;
    }

    public static ArrayList<EmailWrapper> loadJsonEmails(File dir) {
        ArrayList<EmailWrapper> emails = new ArrayList<>();
        File[] files = dir.listFiles();
        for (File file : files)
            if (file.isFile())
                try {
                    emails.add(loadJsonEmail(file, dir.getName()));
                } catch (IOException io) {
                    log.debug("Invalid JSON file \"" + file.getName() + "\"");
                } catch (NullPointerException npe) {
                    log.debug("Invalid fields in file \"" + file.getName() + "\"");
                } catch (Exception e) {
                    log.debug("Unknown error while reading fields in file \"" + file.getName() + "\"");
                }
        return emails;
    }

    public static EmailWrapper loadJsonEmail(File file, String category)
            throws IOException, NullPointerException, Exception {
        final ObjectMapper mapper = new ObjectMapper();
        HashMap<String, ArrayList<HashMap<String, String>>> messageJsonFile = new HashMap<>();
        messageJsonFile = mapper.readValue(file, messageJsonFile.getClass());
        final String id = file.getName();
        final String sender = messageJsonFile.get("from").get(0).get("email");
        final String recipient = messageJsonFile.get("to").get(0).get("email");
        final String subject = String.valueOf(messageJsonFile.get("subject"));
        String contentPlain = readPlainText(messageJsonFile);
        String contentHtml = readHtmlText(messageJsonFile);
        return new EmailWrapper(
                new Email(id, subject, contentPlain, contentHtml, recipient, sender),
                new Category(category));
    }

    private static String readPlainText(HashMap<String, ArrayList<HashMap<String, String>>> messageJsonFile) {
        String contentPlain = "";
        for (int i = 0; i < messageJsonFile.get("attachments").size(); i++)
            if (messageJsonFile.get("attachments").get(i).get("content_type").equals("text/plain"))
                contentPlain += messageJsonFile.get("attachments").get(i).get("content") + " ";
        for (int i = 0; i < messageJsonFile.get("parts").size(); i++)
            if (messageJsonFile.get("parts").get(i).get("content_type").equals("text/plain"))
                contentPlain += messageJsonFile.get("parts").get(i).get("content") + " ";
        return contentPlain;
    }

    private static String readHtmlText(HashMap<String, ArrayList<HashMap<String, String>>> messageJsonFile) {
        String contentHtml = "";
        for (int i = 0; i < messageJsonFile.get("attachments").size(); i++)
            if (messageJsonFile.get("attachments").get(i).get("content_type").equals("text/html"))
                contentHtml += messageJsonFile.get("attachments").get(i).get("content") + " ";
        for (int i = 0; i < messageJsonFile.get("parts").size(); i++)
            if (messageJsonFile.get("parts").get(i).get("content_type").equals("text/html"))
                contentHtml += messageJsonFile.get("parts").get(i).get("content") + " ";
        return contentHtml;
    }
}
