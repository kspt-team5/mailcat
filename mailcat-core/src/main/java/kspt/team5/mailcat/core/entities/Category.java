package kspt.team5.mailcat.core.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Value;

@Value
public class Category {
    private String name;

    @JsonValue
    public String jsonValue() {
        return name;
    }

    @JsonCreator
    public Category(final String name) {
        this.name = name;
    }
}
