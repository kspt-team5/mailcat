package kspt.team5.mailcat.core.entities;

import lombok.Value;

@Value
public class Folder {
    private String name;

    public static Folder fromCategory(final Category category) {
        return new Folder(category.getName());
    }
}
