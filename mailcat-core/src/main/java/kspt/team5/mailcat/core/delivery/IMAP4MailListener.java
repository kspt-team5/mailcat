package kspt.team5.mailcat.core.delivery;

import com.sun.mail.imap.IMAPFolder;
import kspt.team5.mailcat.core.boundaries.MailListener;
import kspt.team5.mailcat.core.entities.Category;
import kspt.team5.mailcat.core.events.IncomingMessageEvent;
import kspt.team5.mailcat.core.events.MovedMessageEvent;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.event.MessageCountEvent;
import javax.mail.event.MessageCountListener;
import javax.mail.internet.MimeMessage;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Slf4j
@AllArgsConstructor
public class IMAP4MailListener extends MailListener {
    private final IMAP4MailService mailService;
    private final String FOLDER_GMAIL = "[Gmail]";
    private final String FOLDER_INBOX = "INBOX";

    private MessageCountListener getMessageCountListener() {
        if (!mailService.isConnected())
            throw new RuntimeException("mailservice is not connected.");
        return new MessageCountListener() {
            @Override
            public void messagesAdded(MessageCountEvent e) {
                Message[] messages = e.getMessages();
                for (Message msg : messages) {
                    try {
                        log.info("[listenForIncomingMessages]: {}", msg.getSubject());
                        eventBus.post(new IncomingMessageEvent(IMAP4MailService.toEmail(msg)));
                    } catch (MessagingException e1) {
                        e1.printStackTrace();
                    }
                }
            }

            @Override
            public void messagesRemoved(MessageCountEvent e) {

            }
        };
    }

    @Override
    protected void listenForIncomingMessages() {
        // use mailService to listen for incoming messages, and, periodically, check if mailService isConnected()
        // post an event like eventBus.post(new IncomingMessageEvent(email));
        log.info("Listener [{}] executed",
                Thread.currentThread().getStackTrace()[1].getMethodName());
        try {
            if (!mailService.getStore().hasCapability("IDLE")) {
                throw new RuntimeException("IDLE is not supported");
            }
            IMAPFolder folder = (IMAPFolder) mailService.getStore().getFolder("INBOX");
            folder.open(Folder.READ_WRITE);
            folder.addMessageCountListener(getMessageCountListener());
            while (!stopped) {
                if (!folder.isOpen())
                    folder.open(Folder.READ_WRITE);
                folder.idle();
            }
        } catch (MessagingException e) {
            log.error("Error in listenForIncomingMessages:", e);
        }
    }

    private void pollFolder(IMAPFolder folder, Map<IMAPFolder, Integer> numMessages) throws MessagingException {
        if (!folder.isOpen())
            folder.open(Folder.READ_ONLY);
        int diff = folder.getMessageCount() - numMessages.get(folder);
        if (diff > 0) {
            long nextUID = folder.getUIDNext();
            Message[] messages = folder.getMessagesByUID(nextUID - diff, nextUID);
            for (Message m : messages) {
                String msgId = ((MimeMessage) m).getMessageID();
                IMAP4MailService.MessageMovement mv = mailService.getMoveHistory().get(msgId);
                if (mv != null) {
                    if (mv.getFolderTo().getFullName().equalsIgnoreCase(folder.getFullName()))
                        continue;
                }
                log.info("[listenForMovedMessages]: {}: {}", folder.getFullName(), m.getSubject());
                eventBus.post(new MovedMessageEvent(IMAP4MailService.toEmail(m),
                        new Category(folder.getFullName())));
            }
            numMessages.put(folder, folder.getMessageCount());
        }
        folder.close(false);
    }

    @Override
    protected void listenForMovedMessages() {
        // use mailService to listen for messages that were moved by the user (NOT BY MailCat!)
        // post an event like eventBus.post(new MovedMessageEvent(email, newCategory));
        log.info("Listener [{}] executed",
                Thread.currentThread().getStackTrace()[1].getMethodName());
        Set<IMAPFolder> folders = new HashSet<>();
        Map<IMAPFolder, Integer> numMessages = new HashMap<>();
        trackedCategories.forEach(category -> {
            try {
                IMAPFolder f = (IMAPFolder) mailService.getStore().getFolder(category.getName());
                folders.add(f);
                numMessages.put(f, f.getMessageCount());
            } catch (MessagingException e) {
                log.error("listenForMovedMessages: can't transfer Folder(category) to javax.mail.Folder", e);
            }
        });

        while (!stopped) {
            for (IMAPFolder folder : folders) {
                try {
                    Thread.sleep(7000);
                    pollFolder(folder, numMessages);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                } catch (MessagingException e) {
                    log.error("Error in listenForMovedMessages:", e);
                    return;
                }
            }
        }
    }
}
