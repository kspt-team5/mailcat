package kspt.team5.mailcat.core.classification;

import kspt.team5.mailcat.core.entities.Category;
import kspt.team5.mailcat.core.entities.Email;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

public abstract class MailClassifier implements Serializable {
    @Getter @Setter
    protected boolean trained = false;

    public abstract Category classify(final Email email);

    public abstract void teach(final Email email, final Category category);
}
