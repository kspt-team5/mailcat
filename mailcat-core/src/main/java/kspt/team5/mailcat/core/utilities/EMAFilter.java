package kspt.team5.mailcat.core.utilities;

import lombok.experimental.UtilityClass;

@UtilityClass
public class EMAFilter { // EMA stands for Exponential Moving Average
    private static final int WINDOW_SIZE = 20; // Он же коэффициент коррекции ФПКК (привет С. А. Нестерову)

    public double nextAverage(final double previousAvg, final double currentVal) {
        return previousAvg + (currentVal - previousAvg) / WINDOW_SIZE;
    }

    public double nextStd(final double previousStd, final double currentVal, final double currentAvg) {
        return previousStd + (Math.pow(currentVal - currentAvg, 2) - previousStd) / WINDOW_SIZE;
    }
}
