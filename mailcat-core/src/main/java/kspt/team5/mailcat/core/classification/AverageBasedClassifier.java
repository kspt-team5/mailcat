package kspt.team5.mailcat.core.classification;

import com.fasterxml.jackson.annotation.JsonProperty;
import kspt.team5.mailcat.core.entities.Category;
import kspt.team5.mailcat.core.entities.Email;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.AbstractMap.SimpleEntry;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.lang.Math.abs;

@NoArgsConstructor
public abstract class AverageBasedClassifier extends MailClassifier {
    public static boolean USE_CONFIDENCE_INTERVALS = true;

    @JsonProperty
    @Getter(value = AccessLevel.PROTECTED)
    private final Map<Category, StatisticalDescription> categoryToStatistics = new HashMap<>();

    @Override
    public Category classify(final Email email) {
        final Double paramValue = calculateParameterValue(email);
        return USE_CONFIDENCE_INTERVALS ?
                findCategoryByConfidenceIntervalHit(paramValue) : findCategoryWithClosestParamValue(paramValue);
    }

    final Category findCategoryWithClosestParamValue(final Double paramValue) {
        return getCategoryToStatistics().entrySet().stream()
                .map(entry -> new SimpleEntry<>(entry.getKey(), abs(entry.getValue().getAverage() - paramValue)))
                .min(Map.Entry.comparingByValue())
                .map(Map.Entry::getKey).orElse(null);
    }

    final Category findCategoryByConfidenceIntervalHit(final Double paramValue) {
        final List<Category> possibleCategories = getCategoryToStatistics().entrySet().stream()
                .filter(entry -> entry.getValue().getConfidenceInterval().contains(paramValue))
                .map(Map.Entry::getKey).collect(Collectors.toList());
        if (possibleCategories.size() != 1) return null;
        else return possibleCategories.get(0);
    }

    @Override
    public void teach(final Email email, final Category category) {
        final double paramValue = calculateParameterValue(email);
        if (!getCategoryToStatistics().containsKey(category)) {
            getCategoryToStatistics().put(category, StatisticalDescription.beginWith(paramValue));
        } else {
            getCategoryToStatistics().get(category).update(paramValue);
        }

    }

    abstract double calculateParameterValue(final Email email);
}
