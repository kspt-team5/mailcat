package kspt.team5.mailcat.core.boundaries;

import kspt.team5.mailcat.core.entities.Config;

public interface ConfigLoader {
    Config getConfig();
}
