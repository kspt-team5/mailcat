package kspt.team5.mailcat.core.interactors;

import kspt.team5.mailcat.core.boundaries.MailService;
import kspt.team5.mailcat.core.classification.MailClassifier;
import kspt.team5.mailcat.core.entities.Category;
import kspt.team5.mailcat.core.entities.Folder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Set;

@Slf4j
@RequiredArgsConstructor
public class CategoryController {
    private final MailService mailService;

    private final MailClassifier classifier;

    void trainClassifier(Set<Category> categories) {
        log.debug("Training MailClassifier...");
        Set<Folder> folders = mailService.getFolders();
        categories.forEach(category -> {
            if(!folders.contains(Folder.fromCategory(category)))
                throw new IllegalStateException();
        });
        categories.forEach(category -> {
            log.debug("Training on emails from folder {}", category.getName());
            mailService.getMessages(Folder.fromCategory(category))
                    .forEach(email -> classifier.teach(email, category));
            classifier.setTrained(true);
        });
        log.debug("MailClassifier trained successfully!");
    }
}
