package kspt.team5.mailcat.core.classification;

import kspt.team5.mailcat.core.entities.Email;
import lombok.NoArgsConstructor;
import org.jsoup.Jsoup;

@NoArgsConstructor
public class HrefCountClassifier extends AverageBasedClassifier {
    private final static int minHrefTagLength = 12;
    // Yo dawg, look at this tag: "<a href="">a"
    // How many symbols are there? Just 12 -- minimum length possible such tag.
    // It's just a letter "a" with a hyperreference to the same document.

    @Override
    double calculateParameterValue(Email email) {
        if (email.getHtmlContent().length() < minHrefTagLength) return 0d;
        return (long) Jsoup.parse(email.getHtmlContent()).select("a[href]").size();
    }
}
