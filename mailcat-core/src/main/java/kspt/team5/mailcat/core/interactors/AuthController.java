package kspt.team5.mailcat.core.interactors;

import kspt.team5.mailcat.core.CannotLoginException;
import kspt.team5.mailcat.core.boundaries.MailService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

@Slf4j
@RequiredArgsConstructor
public class AuthController {
    public final static Object LOCK = new Object();

    private final static int CONNECTION_CHECK_PERIOD_IN_MILLIS = 7000;

    private final MailService mailService;

    private final AtomicBoolean serviceStarted = new AtomicBoolean(false);

    private final AtomicBoolean serviceStoppedAbnormally = new AtomicBoolean(false);

    private ScheduledExecutorService connectionCheckingExecutor;

    public boolean isServiceStarted() {
        return serviceStarted.get();
    }

    public boolean isServiceStoppedAbnormally() {
        return serviceStoppedAbnormally.get();
    }

    public void startMailService(final String host, final int port, final String email, final String password)
            throws CannotLoginException {
        mailService.connect(host, port, email, password);
        serviceStarted.set(true);
        connectionCheckingExecutor = Executors.newSingleThreadScheduledExecutor();
        connectionCheckingExecutor.scheduleAtFixedRate(() -> {
            log.debug("Is MailService connected? {}", mailService.isConnected());
            if (!mailService.isConnected() && isServiceStarted()) stop(true);
        }, CONNECTION_CHECK_PERIOD_IN_MILLIS / 2, CONNECTION_CHECK_PERIOD_IN_MILLIS, TimeUnit.MILLISECONDS);
    }

    public void stopMailService() {
        if (isServiceStarted()) {
            mailService.disconnect();
            stop(false);
        }
    }

    private void stop(final boolean abnormally) {
        connectionCheckingExecutor.shutdownNow();
        serviceStarted.set(false);
        serviceStoppedAbnormally.set(abnormally);
        notifyAwaitingThreads();
    }

    private void notifyAwaitingThreads() {
        synchronized (AuthController.LOCK) {
            AuthController.LOCK.notifyAll();
        }
    }
}
