package kspt.team5.mailcat.core.boundaries;

import com.google.common.eventbus.EventBus;
import kspt.team5.mailcat.core.entities.Category;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;
import java.util.concurrent.ExecutorService;

public abstract class MailListener {
    @Setter
    protected EventBus eventBus;

    @Setter @Getter
    private ExecutorService executor;

    @Setter
    protected Set<Category> trackedCategories;

    protected volatile boolean stopped = false;

    public void stop() {
        stopped = true;
    }

    public void listenInPool() {
        stopped = false;
        executor.submit(this::listenForIncomingMessages);
        executor.submit(this::listenForMovedMessages);
    }

    protected abstract void listenForIncomingMessages();

    protected abstract void listenForMovedMessages();
}
