package kspt.team5.mailcat.core.boundaries;

import kspt.team5.mailcat.core.entities.Category;

import java.util.Set;

public interface CategoryViewer {
    Set<Category> getCategories();
}
