package kspt.team5.mailcat.core.entities;

import java.util.LinkedHashMap;
import java.util.Map;

public class CategorizedEmailsBuffer {
    public final static int CAPACITY = 20;

    private final Map<Email, Category> storage = new LinkedHashMap<>();

    synchronized public void push(Email e, Category c) {
        if (storage.size() >= CAPACITY) {
            Map.Entry<Email, Category> entry = storage.entrySet().iterator().next();
            storage.remove(entry.getKey());
        }
        storage.put(e, c);
    }

    synchronized public Map<Email, Category> getAll() {
        return storage;
    }

    // Singleton impl
    private static CategorizedEmailsBuffer instance = null;

    private CategorizedEmailsBuffer() {}

    public static CategorizedEmailsBuffer getInstance() {
        if (instance == null) {
            synchronized (CategorizedEmailsBuffer.class) {
                if (instance == null)   instance = new CategorizedEmailsBuffer();
            }
        }
        return instance;
    }




}
