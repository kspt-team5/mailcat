package kspt.team5.mailcat.core.utilities;

import kspt.team5.mailcat.core.boundaries.KnowledgeStorage;
import kspt.team5.mailcat.core.boundaries.MailListener;
import kspt.team5.mailcat.core.boundaries.MailService;
import kspt.team5.mailcat.core.classification.MailClassifier;
import kspt.team5.mailcat.core.classification.MailClassifierType;
import kspt.team5.mailcat.core.classification.SimpleMailClassifier;
import kspt.team5.mailcat.core.delivery.FileKnowledgeStorage;
import kspt.team5.mailcat.core.delivery.IMAP4MailListener;
import kspt.team5.mailcat.core.delivery.IMAP4MailService;
import kspt.team5.mailcat.core.entities.Category;
import kspt.team5.mailcat.core.entities.Email;
import kspt.team5.mailcat.core.interactors.ApplicationController;
import kspt.team5.mailcat.core.interactors.AuthController;
import kspt.team5.mailcat.core.interactors.CategoryController;
import kspt.team5.mailcat.core.interactors.MailController;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import org.laughingpanda.beaninject.Inject;

@Slf4j
@Builder
public class ApplicationControllerProvider {
    @Builder.Default
    private MailService mailService = new IMAP4MailService();

    @Builder.Default
    private MailClassifierType mailClassifierType = MailClassifierType.NONE;

    @Builder.Default
    private MailClassifier mailClassifier = null;

    @Builder.Default
    private KnowledgeStorage knowledgeStorage = new FileKnowledgeStorage();

    @Builder.Default
    CategoryController categoryController = null;

    public ApplicationController get() {
        if(mailClassifier == null) {
            mailClassifier = getMailClassifierFromType(mailClassifierType);
        }

        AuthController authController = new AuthController(mailService);
        MailController mailController = new MailController(mailClassifier, mailService);
        MailListener mailListener = createMailListener(mailController);
        CategoryController categoryController = new CategoryController(mailService, mailClassifier);

        ApplicationController applicationController = new ApplicationController();
        Inject.bean(applicationController)
                .with(mailService, authController, mailController, mailListener, categoryController, mailClassifier, knowledgeStorage);

        return applicationController;
    }

    private MailClassifier getMailClassifierFromType(final MailClassifierType mailClassifierType) {
        switch (mailClassifierType) {
            case SIMPLE:
                return createSimpleMailClassifier();
            case NONE:
                return createEmptyMailClassifier(mailClassifierType);
            default:
                throw new IllegalArgumentException("Unsupported mail classifier type: " + mailClassifierType);
        }
    }

    private SimpleMailClassifier createSimpleMailClassifier() {
        return (SimpleMailClassifier) knowledgeStorage.load(SimpleMailClassifier.class);
    }

    private MailClassifier createEmptyMailClassifier(final MailClassifierType mailClassifierType) {
        return new MailClassifier() {
            @Override
            public Category classify(Email email) {
                log.warn("Attempt to use MailClassifier of {}.{} type! Method classify() will return null",
                        mailClassifierType.getClass().getSimpleName(), mailClassifierType);
                return null;
            }

            @Override
            public void teach(Email email, Category category) {
                log.warn("Attempt to use MailClassifier of {}.{} type! Method teach() will do nothing",
                        mailClassifierType.getClass().getSimpleName(), mailClassifierType);
            }
        };
    }

    private MailListener createMailListener(MailController mailController) {
        if (mailService instanceof IMAP4MailService) {
            return new IMAP4MailListener((IMAP4MailService) mailService);
        } else {
            return new MailListener() {
                @Override
                protected void listenForIncomingMessages() {
                    log.warn("Empty MailListener used. Method listenForIncomingMessages() will wait 1 sec and return");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                }

                @Override
                protected void listenForMovedMessages() {
                    log.warn("Empty MailListener used. Method listenForMovedMessages() will wait 1 sec and return");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                }
            };
        }
    }

}
