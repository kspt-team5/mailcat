package kspt.team5.mailcat.core.boundaries;

import kspt.team5.mailcat.core.classification.MailClassifier;

public interface KnowledgeStorage {
    void store(final MailClassifier base);

    MailClassifier load(final Class<? extends MailClassifier> baseClass);
}
