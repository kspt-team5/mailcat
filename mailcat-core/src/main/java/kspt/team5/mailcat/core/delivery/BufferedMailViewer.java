package kspt.team5.mailcat.core.delivery;

import kspt.team5.mailcat.core.boundaries.MailViewer;
import kspt.team5.mailcat.core.entities.CategorizedEmailsBuffer;
import kspt.team5.mailcat.core.entities.Category;
import kspt.team5.mailcat.core.entities.Email;

import java.util.Map;

public class BufferedMailViewer implements MailViewer {

    @Override
    public Map<Email, Category> getLastCategorizedEmails() {
        return CategorizedEmailsBuffer.getInstance().getAll();
    }
}