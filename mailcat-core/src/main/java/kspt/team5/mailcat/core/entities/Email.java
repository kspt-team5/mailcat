package kspt.team5.mailcat.core.entities;

import lombok.ToString;
import lombok.Value;

@Value
@ToString(exclude = {"plainContent", "htmlContent"})
public class Email {
    private String id;

    private String subject;

    private String plainContent;

    private String htmlContent;

    private String recipient;

    private String sender;

    public static Email empty() {
        return new Email("0", "", "", "", "", "");
    }
}

