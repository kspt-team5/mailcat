package kspt.team5.mailcat.core.classification;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Preconditions;
import com.google.common.collect.Range;
import kspt.team5.mailcat.core.utilities.EMAFilter;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
class StatisticalDescription {
    @Getter
    @JsonProperty
    private double average = 0.0;

    @JsonProperty
    private double squaredStd = 0.0;

    @JsonProperty
    private int sampleSize = 0;

    double getStd() {
        return Math.sqrt(squaredStd);
    }

    static StatisticalDescription beginWith(final double initialVal) {
        StatisticalDescription stat = new StatisticalDescription();
        stat.update(initialVal);
        return stat;
    }

    void update(final double currentValue) {
        sampleSize++;
        if (sampleSize == 1) {
            average = currentValue;
            squaredStd = 0.0;
        } else {
            average = EMAFilter.nextAverage(average, currentValue);
            squaredStd = EMAFilter.nextStd(squaredStd, currentValue, average);
        }
    }

    Range<Double> getConfidenceInterval() {
        if (sampleSize < 2) {
            return Range.closed(average, average);
        } else {
            final double radius = Math.sqrt(squaredStd) * tinv090(sampleSize) / Math.sqrt(sampleSize);
            return Range.closed(average - radius, average + radius);
        }
    }

    private static double tinv090(int sampleSize) {
        Preconditions.checkArgument(sampleSize > 1);
        if (sampleSize > 30) {
            return 1.67;
        } else {
            return TINV_Q090[(sampleSize - 1) - 1];
        }
    }

    private static final double[] TINV_Q090 =
            // Inverse Student's distribution for k = 1, 2, ... , 29 (i.e. N = 2, 3, ... , 30) and Q = 0.90
            { 6.31, 2.92, 2.35, 2.13, 2.02, 1.94, 1.89, 1.86, 1.83, 1.81, 1.80, 1.78, 1.77, 1.76, 1.75,
                    1.75, 1.74, 1.73, 1.73, 1.72, 1.72, 1.72, 1.71, 1.71, 1.71, 1.71, 1.70, 1.70, 1.70 };
}
