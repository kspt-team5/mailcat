package kspt.team5.mailcat.core.boundaries;

import java.util.Map;

public interface SettingsViewer {
    Map<String, Object> getSettings();
}
