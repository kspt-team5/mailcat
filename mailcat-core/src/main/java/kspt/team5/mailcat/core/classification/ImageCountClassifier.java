package kspt.team5.mailcat.core.classification;

import kspt.team5.mailcat.core.entities.Email;
import org.jsoup.Jsoup;

public class ImageCountClassifier extends AverageBasedClassifier {
    private final static int minImgTagLength = 14;
    // Yo dawg, look at this tag: "<img src="/a">"
    // How many symbols are there? Just 14 -- minimum length possible such tag.
    // It's the tag for insert an image from path "/a".

    @Override
    double calculateParameterValue(Email email) {
        if(email.getHtmlContent().length() < minImgTagLength) return 0.0;
        return (long) Jsoup.parse(email.getHtmlContent()).select("img[src]").size();
    }
}