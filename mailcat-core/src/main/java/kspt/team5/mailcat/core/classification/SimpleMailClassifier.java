package kspt.team5.mailcat.core.classification;

import kspt.team5.mailcat.core.entities.Category;
import kspt.team5.mailcat.core.entities.Email;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

public class SimpleMailClassifier extends MailClassifier {
    @Getter
    private Map<String, Category> knowledgeBase = new HashMap<>();

    @Override
    public Category classify(Email email) {
        return knowledgeBase.get(email.getSender());
    }

    @Override
    public void teach(Email email, Category category) {
        knowledgeBase.put(email.getSender(), category);
    }
}
