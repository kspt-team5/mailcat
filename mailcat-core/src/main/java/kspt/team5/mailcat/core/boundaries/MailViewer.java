package kspt.team5.mailcat.core.boundaries;

import kspt.team5.mailcat.core.entities.Category;
import kspt.team5.mailcat.core.entities.Email;

import java.util.Map;

public interface MailViewer {
    Map<Email, Category> getLastCategorizedEmails();
}
