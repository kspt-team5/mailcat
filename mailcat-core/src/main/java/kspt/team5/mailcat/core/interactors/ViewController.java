package kspt.team5.mailcat.core.interactors;

import kspt.team5.mailcat.core.boundaries.CategoryViewer;
import kspt.team5.mailcat.core.boundaries.MailViewer;
import kspt.team5.mailcat.core.boundaries.SettingsViewer;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class ViewController {
    @Getter
    private final CategoryViewer categoryViewer;

    @Getter
    private final MailViewer mailViewer;

    @Getter
    private final SettingsViewer settingsViewer;
}
