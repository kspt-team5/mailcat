package kspt.team5.mailcat.core.entities;

import lombok.Value;

import java.util.Set;

@Value
public class Config {
    private String email;

    private String password;

    private Set<Category> categories;

    private Integer learningThreshold;

    private String host;

    private int port;
}
