package kspt.team5.mailcat.core.classification;

import com.fasterxml.jackson.annotation.JsonProperty;
import kspt.team5.mailcat.core.entities.Category;
import kspt.team5.mailcat.core.entities.Email;

import java.util.HashMap;
import java.util.Map;

public class SenderClassifier extends MailClassifier {
    @JsonProperty
    private final Map<String, Map<Category, Integer>> knowledgeBase = new HashMap<>();

    @Override
    public Category classify(Email email) {
        if(!knowledgeBase.containsKey(email.getSender())) return null;
        else return knowledgeBase.get(email.getSender()).entrySet().stream()
                .max(Map.Entry.comparingByValue())
                .map(Map.Entry::getKey).orElse(null);
    }

    @Override
    public void teach(Email email, Category category) {
        final String sender = email.getSender();
        if(knowledgeBase.containsKey(sender)) {
            knowledgeBase.get(sender).compute(category, (key, oldValue) -> oldValue == null ? 1 : ++oldValue);
        } else {
            knowledgeBase.put(sender, new HashMap<>());
            knowledgeBase.get(sender).put(category, 1);
        }
    }
}
