package kspt.team5.mailcat.core.delivery;

import kspt.team5.mailcat.core.boundaries.CategoryViewer;
import kspt.team5.mailcat.core.entities.CategorizedEmailsBuffer;
import kspt.team5.mailcat.core.entities.Category;
import kspt.team5.mailcat.core.entities.Config;
import kspt.team5.mailcat.core.entities.Email;
import lombok.AllArgsConstructor;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@AllArgsConstructor
public class TrivialCategoryViewer implements CategoryViewer {

    private final Config config;

    @Override
    public Set<Category> getCategories() {
        return config.getCategories();
    }
}
