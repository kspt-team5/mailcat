package kspt.team5.mailcat.core.classification;

import kspt.team5.mailcat.core.entities.Category;
import kspt.team5.mailcat.core.entities.Email;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
public class ClassificationCouncil extends MailClassifier {
    public static final Map<Class, Integer> CLASS_TO_NULLSCOUNT = new HashMap<>();

    private final Function<List<Category>, Category> solver = MajoritySolver::solve;

    @Getter
    private final List<MailClassifier> members = new ArrayList<>();

    public void registerMember(final MailClassifier classifier) {
        members.add(classifier);
    }

    public void registerMembers(final MailClassifier... classifiers) {
        for (MailClassifier c : classifiers) {
            registerMember(c);
        }
    }

    @Override
    public Category classify(final Email email) {
        log.debug("Classification Council voting is started ({} members)...", members.size());
        final List<Category> decisions =
                members.stream().map(member -> {
                    final Category result = member.classify(email);
                    log.debug("{}'s vote is: {}",
                            member.getClass().getSimpleName(), result == null ? "<uncertain>" : result.getName());
                    if (result == null) incCounter(member.getClass());
                    return result;
                }).collect(Collectors.toList());
        final Category finalDecision = solver.apply(decisions);
        log.debug("The final decision is: {}", finalDecision == null ? "<uncertain>" : finalDecision.getName());
        return finalDecision;
    }

    @Override
    public void teach(final Email email, final Category category) {
        members.forEach(member -> member.teach(email, category));
    }

    @Override
    public void setTrained(boolean trained) {
        members.forEach(member -> member.setTrained(trained));
        super.setTrained(trained);
    }

    private static void incCounter(Class clazz) {
        if (CLASS_TO_NULLSCOUNT.containsKey(clazz)) {
            CLASS_TO_NULLSCOUNT.put(clazz, CLASS_TO_NULLSCOUNT.get(clazz) + 1);
        } else {
            CLASS_TO_NULLSCOUNT.put(clazz, 1);
        }
    }
}
