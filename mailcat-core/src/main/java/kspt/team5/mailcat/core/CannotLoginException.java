package kspt.team5.mailcat.core;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class CannotLoginException extends RuntimeException {
    CannotLoginException(final String msg) {
        super(msg);
    }
}