package kspt.team5.mailcat.core.classification;

import lombok.ToString;

@ToString
public enum MailClassifierType {
    NONE, SIMPLE, WEIGHT_BASED, VECTOR_BASED;

    static MailClassifierType of(final String name) {
        switch (name) {
            case "simple":
                return SIMPLE;
            case "weight-based":
                return WEIGHT_BASED;
            case "vector-based":
                return VECTOR_BASED;
            case "none":
                throw new IllegalArgumentException("Construction of " + NONE + " type by name is prohibited");
            default:
                throw new IllegalArgumentException("Illegal mail classifier type name: " + name);
        }
    }
}
