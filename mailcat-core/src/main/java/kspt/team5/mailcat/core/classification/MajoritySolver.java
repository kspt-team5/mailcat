package kspt.team5.mailcat.core.classification;

import com.google.common.base.Preconditions;
import kspt.team5.mailcat.core.entities.Category;
import lombok.experimental.UtilityClass;

import java.util.*;
import java.util.stream.Collectors;

@UtilityClass
class MajoritySolver {
    static final double MAJORITY_COEFF = 2.0 / 3.0;

    Category solve(final List<Category> decisions) {
        final List<Category> certainDecisions = decisions.stream().filter(Objects::nonNull).collect(Collectors.toList());
        return certainDecisions.isEmpty() || isMajorityUncertain(decisions)
                ? null : findCategoryWithMaxVotes(certainDecisions);
    }

    private boolean isMajorityUncertain(List<Category> decisions) {
        return Collections.frequency(decisions, null) > MAJORITY_COEFF * (double) decisions.size();
    }

    private Category findCategoryWithMaxVotes(final List<Category> certainDecisions) {
        final Map<Category, Integer> votesDistribution = findVotesDistribution(certainDecisions);
        final Category favourableCategory = findFavourableCategory(votesDistribution);
        Preconditions.checkNotNull(favourableCategory, "Category is null after filtering out nulls!");
        return Collections.frequency(votesDistribution.values(), votesDistribution.get(favourableCategory)) == 1 ?
                favourableCategory : null;
    }

    private Map<Category, Integer> findVotesDistribution(final List<Category> certainDecisions) {
        final Map<Category, Integer> votesDistribution = new HashMap<>();
        final Set<Category> variants = new HashSet<>(certainDecisions);
        for (Category category : variants) {
            votesDistribution.put(category, Collections.frequency(certainDecisions, category));
        }
        return votesDistribution;
    }

    private Category findFavourableCategory(final Map<Category, Integer> votesDistribution) {
        return votesDistribution.entrySet().stream()
                .max(Comparator.comparing(Map.Entry::getValue))
                .map(Map.Entry::getKey).orElse(null);
    }
}
