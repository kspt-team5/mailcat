package kspt.team5.mailcat.core.interactors;

import com.google.common.eventbus.AllowConcurrentEvents;
import com.google.common.eventbus.DeadEvent;
import com.google.common.eventbus.Subscribe;
import kspt.team5.mailcat.core.boundaries.MailService;
import kspt.team5.mailcat.core.classification.MailClassifier;
import kspt.team5.mailcat.core.entities.CategorizedEmailsBuffer;
import kspt.team5.mailcat.core.entities.Category;
import kspt.team5.mailcat.core.entities.Email;
import kspt.team5.mailcat.core.entities.Folder;
import kspt.team5.mailcat.core.events.IncomingMessageEvent;
import kspt.team5.mailcat.core.events.MovedMessageEvent;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
public class MailController {
    private final MailClassifier mailClassifier;

    private final MailService mailService;

    @Subscribe
    @AllowConcurrentEvents
    public void handleIncomingMessageEvent(final IncomingMessageEvent event) {
        log.debug("Handling event {}", event);
        final Email email = event.getEmail();
        final Category classificationResult = mailClassifier.classify(email);
        if(classificationResult != null) {
            mailService.moveEmail(email, Folder.fromCategory(classificationResult));
            log.debug("Message {} moved to {}", email.getSubject(), classificationResult);
            CategorizedEmailsBuffer.getInstance().push(email,classificationResult);
        } else {
            log.debug("Message {} left uncategorized", email.getSubject());
        }
    }

    @Subscribe
    public void handleMovedMessageEvent(final MovedMessageEvent event) {
        log.debug("Handling event {}", event);
        mailClassifier.teach(event.getEmail(), event.getNewCategory());
    }

    @Subscribe
    public void handleDeadEvent(final DeadEvent deadEvent) {
        log.warn("Unhandled event detected: {}", deadEvent.getEvent());
    }
}
