package kspt.team5.mailcat.core.delivery;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import kspt.team5.mailcat.core.boundaries.KnowledgeStorage;
import kspt.team5.mailcat.core.classification.ClassificationCouncil;
import kspt.team5.mailcat.core.classification.MailClassifier;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;

@Slf4j
@RequiredArgsConstructor
public class FileKnowledgeStorage implements KnowledgeStorage {
    private static final String STORAGE_FILE_NAME = "kb.json";

    private static final ObjectMapper mapper = new ObjectMapper();

    private final File file;

    static {
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        mapper.enableDefaultTyping();
    }

    public FileKnowledgeStorage() {
        file = new File(STORAGE_FILE_NAME);
    }

    @Override
    public void store(final MailClassifier classifier) {
        try {
            if (classifier instanceof ClassificationCouncil) {
                mapper.writeValue(file, (ClassificationCouncil) classifier);
            } else mapper.writeValue(file, classifier);
        } catch (IOException e) {
            log.error("Cannot save mail classifier!", e);
        }
    }

    @Override
    public MailClassifier load(final Class<? extends MailClassifier> classifierClass) {
        try {
            return mapper.readValue(file, classifierClass);
        } catch (IOException e) {
            log.error("Cannot load mail classifier: {}", e.getMessage());
            log.warn("New mail classifier will have no knowledge");
            return getEmptyMailClassifier(classifierClass);
        }
    }

    private MailClassifier getEmptyMailClassifier(Class<? extends MailClassifier> classifierClass) {
        try {
            return classifierClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
            return null;
        }
    }
}
