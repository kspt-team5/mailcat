package kspt.team5.mailcat.core.boundaries;

import kspt.team5.mailcat.core.entities.Email;
import kspt.team5.mailcat.core.entities.Folder;

import java.util.Set;

public interface MailService {
    void connect(String host, int port, String user, String password);

    boolean isConnected();

    void createFolder(Folder newFolder);

    Set<Email> getInbox();

    Set<Email> getMessages(Folder folder);

    Set<Folder> getFolders();

    void moveEmail(Email email, Folder targetFolder);

    Folder getFolderFor(Email email);

    void disconnect();
}
