package kspt.team5.mailcat.core.events;

import kspt.team5.mailcat.core.entities.Category;
import kspt.team5.mailcat.core.entities.Email;
import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor
public class MovedMessageEvent {
    private Email email;

    private Category previousCategory;

    private Category newCategory;

    public MovedMessageEvent(final Email email, final Category newCategory) {
        this.email = email;
        this.newCategory = newCategory;
        this.previousCategory = null;
    }
}
