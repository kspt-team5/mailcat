package kspt.team5.mailcat.core;

import kspt.team5.mailcat.core.boundaries.ConfigLoader;
import kspt.team5.mailcat.core.delivery.TypesafeConfigLoader;
import kspt.team5.mailcat.core.utilities.ApplicationControllerProvider;

import java.io.File;

public class ApplicationRunner {
    private static final String CONFIG_FILE_NAME = "application.conf";

    private static final File configFile =
            new File(System.getProperty("user.dir") + File.separator + ApplicationRunner.CONFIG_FILE_NAME);

    public static void main(String[] args) {
        ConfigLoader configLoader = new TypesafeConfigLoader(ApplicationRunner.configFile);
        ApplicationControllerProvider.builder().build().get().run(configLoader.getConfig());
    }
}