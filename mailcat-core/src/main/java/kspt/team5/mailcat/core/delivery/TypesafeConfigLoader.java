package kspt.team5.mailcat.core.delivery;

import com.typesafe.config.ConfigException;
import com.typesafe.config.ConfigFactory;
import kspt.team5.mailcat.core.boundaries.ConfigLoader;
import kspt.team5.mailcat.core.entities.Category;
import kspt.team5.mailcat.core.entities.Config;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Slf4j
@AllArgsConstructor
public class TypesafeConfigLoader implements ConfigLoader {
    final private File configFile;

    @Override
    public Config getConfig() throws ConfigException {
        final com.typesafe.config.Config config = ConfigFactory.parseFile(configFile);
        config.checkValid(ConfigFactory.defaultReference().getConfig("mailcat"));
        final String email = config.getString("email");
        final String pass = config.getString("password");
        final List<String> categories = config.getStringList("categories");
        final int learningThreshold = config.getInt("learningThreshold");
        final String host = config.getString("host");
        final int port = config.getInt("port");
        return new Config(email, pass, toCategorySet(categories), learningThreshold, host, port);
    }

    private Set<Category> toCategorySet(List<String> categories) {
        final Set<Category> result = new HashSet<>();
        for(String cat : categories) {
            result.add(new Category(cat));
        }
        if(categories.size() != result.size()) {
            log.warn("Duplicate categories detected in 'application.conf' file!");
        }
        return result;
    }
}
