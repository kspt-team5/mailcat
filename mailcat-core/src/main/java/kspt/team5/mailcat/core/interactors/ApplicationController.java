package kspt.team5.mailcat.core.interactors;

import com.google.common.base.Preconditions;
import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.EventBus;
import kspt.team5.mailcat.core.CannotLoginException;
import kspt.team5.mailcat.core.boundaries.KnowledgeStorage;
import kspt.team5.mailcat.core.boundaries.MailListener;
import kspt.team5.mailcat.core.boundaries.MailService;
import kspt.team5.mailcat.core.classification.MailClassifier;
import kspt.team5.mailcat.core.delivery.BufferedMailViewer;
import kspt.team5.mailcat.core.delivery.ConnectionSettingsViewer;
import kspt.team5.mailcat.core.delivery.TrivialCategoryViewer;
import kspt.team5.mailcat.core.entities.Config;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

@Slf4j
@NoArgsConstructor
public class ApplicationController {
    private final static int EVENT_BUS_THREAD_POOL_CAPACITY = 2;

    private final static int LISTENERS_THREAD_POOL_CAPACITY = 2;

    private Config config;

    @SuppressWarnings("unused") private KnowledgeStorage knowledgeStorage;

    @SuppressWarnings("unused") private MailService mailService;

    @SuppressWarnings("unused") private AuthController authController;

    @SuppressWarnings("unused") private CategoryController categoryController;

    @SuppressWarnings("unused") private MailController mailController;

    @SuppressWarnings("unused") private MailClassifier mailClassifier;

    @SuppressWarnings("unused") private MailListener mailListener;

    private ExecutorService listenersThreadPool;

    private ExecutorService eventBusThreadPool;

    @Getter(value = AccessLevel.PUBLIC, lazy = true)
    private final ViewController viewController = createViewController();

    private final AtomicBoolean started = new AtomicBoolean(false);

    public boolean isRun() {
        return started.get();
    }

    public void run(final Config config) {
        log.info("Running the application");
        try {
            tryToRunApplication(config);
        } catch (CannotLoginException e) {
            log.error("Cannot login: "+ e.getMessage(), e);
        } catch (InterruptedException e) {
            log.info("Terminating the application");
            Thread.currentThread().interrupt();
        } finally {
            shutdown();
        }
    }

    private void tryToRunApplication(final Config config) throws CannotLoginException, InterruptedException {
        this.config = config;
        authController.startMailService(config.getHost(), config.getPort(), config.getEmail(), config.getPassword());
        if(!mailClassifier.isTrained()) categoryController.trainClassifier(config.getCategories());
        startEventSubsystem(config);
        started.set(true);
        awaitTermination();
    }

    private void awaitTermination() throws InterruptedException {
        listenersThreadPool.shutdown();
        synchronized (AuthController.LOCK) {
            while (authController.isServiceStarted()) AuthController.LOCK.wait();
        }
        if (authController.isServiceStoppedAbnormally()) {
            throw new RuntimeException("MailService stopped abnormally");
        }
        listenersThreadPool.awaitTermination(Integer.MAX_VALUE, TimeUnit.SECONDS);
    }

    private void startEventSubsystem(final Config config) {
        listenersThreadPool = Executors.newFixedThreadPool(LISTENERS_THREAD_POOL_CAPACITY);
        eventBusThreadPool = Executors.newFixedThreadPool(EVENT_BUS_THREAD_POOL_CAPACITY);
        EventBus eventBus = new AsyncEventBus(eventBusThreadPool);
        eventBus.register(mailController);
        mailListener.setEventBus(eventBus);
        mailListener.setExecutor(listenersThreadPool);
        mailListener.setTrackedCategories(config.getCategories());
        mailListener.listenInPool();
    }

    private ViewController createViewController() {
        Preconditions.checkNotNull(config, "Cannot create view controllers: the application is not run.");
        return new ViewController(
                new TrivialCategoryViewer(config),
                new BufferedMailViewer(),
                new ConnectionSettingsViewer(config)
        );
    }

    public void stop() {
        if(isRun()) {
            log.info("Stopping the application");
            listenersThreadPool.shutdownNow();
            eventBusThreadPool.shutdownNow();
            mailListener.stop();
            authController.stopMailService();
        } else {
            log.warn("Attempt to stop {} which was not run or already stopped", this.getClass().getSimpleName());
        }
    }

    private void shutdown() {
        mailListener.stop();
        authController.stopMailService();
        knowledgeStorage.store(mailClassifier);
        started.set(false);
        log.info("Shutting down");
    }
}
