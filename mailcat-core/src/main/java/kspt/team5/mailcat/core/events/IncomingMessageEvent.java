package kspt.team5.mailcat.core.events;

import kspt.team5.mailcat.core.entities.Email;
import lombok.Value;

@Value
public class IncomingMessageEvent {
    private Email email;
}
