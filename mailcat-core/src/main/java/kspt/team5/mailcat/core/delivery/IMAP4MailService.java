package kspt.team5.mailcat.core.delivery;

import com.google.common.base.Preconditions;
import com.sun.mail.imap.IMAPFolder;
import com.sun.mail.imap.IMAPStore;
import kspt.team5.mailcat.core.boundaries.MailService;
import kspt.team5.mailcat.core.entities.Email;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;

import javax.mail.*;
import javax.mail.event.MessageCountAdapter;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.SearchTerm;
import java.io.IOException;
import java.util.*;

@Slf4j
public class IMAP4MailService implements MailService {

    private Properties props;

    @Getter
    private IMAPStore store;
    @Getter
    private Map<String, MessageMovement> moveHistory = new HashMap<>();

    private Session session = null;

    final String host = "imap.gmail.com";
    final int port = 993;
    final String user = "team5kspt@gmail.com";
    final String password = "team5password";
    private Message foundMessage = null; // returned from findMessage method
    @Getter
    private int nextMsgMoveId = 1;


    private static class MessageContent {
        @Getter
        private StringBuilder sbPlain;
        @Getter
        private StringBuilder sbHtml;

        MessageContent() {
            sbPlain = new StringBuilder();
            sbHtml = new StringBuilder();
        }

        public void appendPlainStringBuffer(String s) {
            sbPlain.append(s);
        }

        public void appendHtmlStringBuffer(String s) {
            sbHtml.append(s);
        }
    }

    @AllArgsConstructor
    final class MessageMovement {
        @Getter
        @Setter
        private int mvId;
        @Getter
        @Setter
        private Folder folderFrom;
        @Getter
        @Setter
        private Folder folderTo;
    }

    @Override
    public void connect(String host, int port, String user, String password) {
        try {
            props = new Properties();
            props.setProperty("mail.store.protocol", "imaps");
            props.put("mail.imap.host", host);
            props.put("mail.imap.port", String.valueOf(port));
            props.put("mail.imap.ssl.enable", "true");

            session = Session.getInstance(props, new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(user, password);
                }
            });
            store = (IMAPStore) session.getStore("imaps");

            store.connect(host, user, password);
            if (isConnected())
                log.debug("Successfully connected to {}:{} as {}", host, port, user);
            else log.error("Cannot connect to IMAP4 server");

        } catch (Exception e) {
            log.error("Cannot connect to IMAP4 server", e);
        }
    }

    public boolean isConnected() {
        Preconditions.checkNotNull(store);
        return store.isConnected();
    }

    @Override
    public void createFolder(kspt.team5.mailcat.core.entities.Folder newFolder) {
        // NOTE: You cant place nested folders inside default INBOX folder,
        // you can do it only for your own created folders(called "Labels" in Gmail)
        try {
            Folder parent = store.getDefaultFolder(); // top level of hierarchy
            String newFolderName = newFolder.getName();
            createFolder(parent, newFolderName);
        } catch (MessagingException e) {
            log.error("Cannot get default folder", e);
        }
    }

    private void createFolder(Folder parent, String folderName) {
        try {
            Folder newFolder = parent.getFolder(folderName);

            if (!newFolder.exists()) newFolder.create(Folder.HOLDS_MESSAGES);
            else log.warn("The folder is already exists!");
        } catch (MessagingException e) {
            log.error("Error while creating new folder: " + e.getMessage(), e);
        }
    }

    @Override
    public Set<Email> getInbox() {
        return getMessages(new kspt.team5.mailcat.core.entities.Folder("INBOX"));
    }


    static Email toEmail(Message m) {
        try {
            StringBuilder recipients = new StringBuilder();
            StringBuilder sender = new StringBuilder();

            for (Address a : m.getAllRecipients())
                recipients.append(((InternetAddress) a).getAddress());
            for (Address a : m.getFrom())
                sender.append(((InternetAddress) a).getAddress());
            MessageContent content = getContentFromMessage(m);
            return new Email(((MimeMessage) m).getMessageID(), m.getSubject(), content.getSbPlain().toString(),
                    content.getSbHtml().toString(), recipients.toString(), sender.toString());
        } catch (MessagingException e) {
            log.warn("Cannot read email headers correctly. Message has been ignored.", e);
        }
        return null;
    }

    private static MessageContent getContentFromMessage(Message message) {
        MessageContent content = new MessageContent();
        try {
            if (message.isMimeType("text/plain")) {
                content.appendPlainStringBuffer(message.getContent().toString());
            } else if (message.isMimeType("multipart/*")) {
                MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
                getTextFromMimeMultipart(mimeMultipart, content);
            } else if (message.isMimeType("text/html")) {
                content.appendHtmlStringBuffer("\n" + (String) message.getContent());
            }
        } catch (MessagingException e) {
            log.error("Cannot read the email content. The message has been ignored.");
            log.debug("DEBUG: Cannot read the email content. The message has been ignored.", e);
        } catch (IOException e) {
            log.error("Cannot read the email content. The message has been ignored.");
            log.debug("DEBUG: Cannot read the email content. The message has been ignored.", e);
        }
        return content;
    }

    private static void getTextFromMimeMultipart(MimeMultipart mimeMultipart, MessageContent content)
            throws MessagingException, IOException {
        int count = 0;
        try {
            count = mimeMultipart.getCount();
            for (int i = 0; i < count; i++) {
                BodyPart bodyPart = mimeMultipart.getBodyPart(i);
                if (bodyPart.isMimeType("text/plain")) {
                    String toAppend = "\n" + bodyPart.getContent();
                    if (toAppend.length() > 1)
                        content.appendPlainStringBuffer(toAppend);
                } else if (bodyPart.isMimeType("text/html")) {
                    content.appendHtmlStringBuffer("\n" + (String) bodyPart.getContent());
                } else if (bodyPart.getContent() instanceof MimeMultipart) {
                    getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent(), content);
                }
            }
        } catch (MessagingException e) {
            throw e;
        } catch (IOException e) {
            throw e;
        }
    }


    @Override
    public Set<Email> getMessages(kspt.team5.mailcat.core.entities.Folder folder) {
        Set<Email> emails = new HashSet<>();
        try {
            Folder f = store.getFolder(folder.getName());
            // for Gmail: [Gmail] folder doesnt' exist and can't be opened.
            // However, folders as [Gmail]/Sent, [Gmail]/Spam exist and can be read and opened.
            // For some reason, folder.exist() checking doesn't work correctly this case.
            if (f.getFullName().equalsIgnoreCase("[Gmail]")) {
                log.debug("[Gmail] folder is ignored.");
                return emails;
            }
            f.open(Folder.READ_ONLY);
            Message[] messages = f.getMessages();
            for (Message m : messages) {
                Email email = toEmail(m);
                if (email != null) emails.add(toEmail(m));
            }
        } catch (MessagingException e) {
            log.error("getMessages: cannot find folder", e);
        }
        return emails;
    }

    @Override
    public Set<kspt.team5.mailcat.core.entities.Folder> getFolders() {
        Set<kspt.team5.mailcat.core.entities.Folder> set = new HashSet<>();
        try {
            Folder[] javaxFolders = store.getDefaultFolder().list();
            getSubfolders(set, javaxFolders);
        } catch (MessagingException e) {
            log.error("getFolders: cannot  store.getDefaultFolder() method. Probably connection was lost.", e);
        }
        return set;
    }

    private void getSubfolders(Set<kspt.team5.mailcat.core.entities.Folder> set, Folder[] folders) {
        try {
            if (folders.length == 0)
                return;
            for (Folder f : folders) {
                String parentName = f.getParent().getName();
                String sep = (parentName.isEmpty()) ? "" : "/";
                set.add(new kspt.team5.mailcat.core.entities.Folder(parentName + sep + f.getName()));
                getSubfolders(set, f.list());
            }

        } catch (MessagingException e) {
            log.error("getSubfolders: folder not found");
        }
    }


    @Override
    public void moveEmail(Email email, kspt.team5.mailcat.core.entities.Folder targetFolder) {
        try {
            Folder folderFrom = store.getFolder(getFolderFor(email).getName());
            Folder folderTo = store.getFolder(targetFolder.getName());
            if (!folderFrom.exists()) throw new MessagingException("Source folder doesn't exist");
            if (!folderTo.exists()) throw new MessagingException("Target folder doesn't exist");
            moveHistory.put(email.getId(), new MessageMovement(nextMsgMoveId++, folderFrom, folderTo));
            moveEmail(foundMessage, folderFrom, folderTo);
            log.info("moveEmail: Email was moved successfully.");
        } catch (MessagingException e) {
            log.error("Cannot move email", e);
        } catch (NullPointerException e) {
            log.error("Cannot find message in any folder. MOVE operation has been failed.", e);
        }
    }

    private void moveEmail(Message msg, Folder folderFrom, Folder folderTo) {
        try {
            if (!folderFrom.isOpen())
                folderFrom.open(Folder.READ_WRITE);
            if (!folderTo.isOpen())
                folderTo.open(Folder.READ_WRITE);
            log.info("moveEmail: Target and source folder were opened successfully.");
            Message[] messages = new Message[1];
            messages[0] = folderFrom.getMessage(msg.getMessageNumber());
            folderFrom.copyMessages(messages, folderTo);
            // mark copied messages from source folder as \deleted
            folderFrom.setFlags(messages, new Flags(Flags.Flag.DELETED), true);
            // expunge: delete messages marked as \deleted
            folderFrom.expunge();
            folderFrom.close(false);
            folderTo.close(false);
            log.info("moveEmail: Folder was closed successfully.");
        } catch (MessagingException e) {
            log.error("Cannot move email", e);
        } catch (IllegalStateException e) {
            log.error("Folder is not opened.", e);
        }

    }

    @Override
    public kspt.team5.mailcat.core.entities.Folder getFolderFor(Email email) throws NullPointerException {
        try {
            Message msg = findMessage(email);
            Folder folderName = msg.getFolder();
            return new kspt.team5.mailcat.core.entities.Folder(folderName.getFullName());
        } catch (NullPointerException e) {
            throw e;
        }
    }

    private Message findMessage(Email email) throws NullPointerException {
        try {

            SearchTerm term = new SearchTerm() {
                @Override
                public boolean match(Message msg) {
                    try {
                        if (email.getId().equalsIgnoreCase(((MimeMessage) msg).getMessageID()))
                            return true;
                    } catch (MessagingException e) {

                    } catch(NullPointerException e) {
                        throw e;
                    }
                    return false;
                }
            };
            Set<kspt.team5.mailcat.core.entities.Folder> folders = getFolders();
            for (kspt.team5.mailcat.core.entities.Folder folder : folders) {
                if (folder.getName().contains("[Gmail]")) continue;
                Folder f = store.getFolder(folder.getName());
                if (!f.exists()) continue;
                if (!f.isOpen())
                    f.open(Folder.READ_WRITE);
                Message[] messages = f.search(term);
                f.close(false);
                if (messages.length > 0) {
                    foundMessage = messages[0];
                    return messages[0];
                }
            }
        } catch (MessagingException e) {
            NullPointerException npe = new NullPointerException();
            npe.initCause(e);
            throw npe;
        }
        log.error("Folder was not found for asked Email.");
        return null;
    }

    @Override
    public void disconnect() {
        try {
            store.close();
            log.debug("Disconnected from the mail service");
        } catch (MessagingException e) {
            log.error("Error while store.close()", e);
        }
    }
}
