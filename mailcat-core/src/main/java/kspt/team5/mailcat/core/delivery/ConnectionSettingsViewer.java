package kspt.team5.mailcat.core.delivery;

import kspt.team5.mailcat.core.boundaries.SettingsViewer;
import kspt.team5.mailcat.core.entities.Config;
import lombok.AllArgsConstructor;

import java.util.LinkedHashMap;
import java.util.Map;

@AllArgsConstructor
public class ConnectionSettingsViewer implements SettingsViewer {
    private final Config config;

    @Override
    public Map<String, Object> getSettings() {
        final Map<String, Object> settings = new LinkedHashMap<>();
        settings.put("host", config.getHost());
        settings.put("port", config.getPort());
        settings.put("email", config.getEmail());
        return settings;
    }
}
