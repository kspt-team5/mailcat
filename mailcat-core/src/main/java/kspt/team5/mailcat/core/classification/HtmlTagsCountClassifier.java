package kspt.team5.mailcat.core.classification;

import kspt.team5.mailcat.core.entities.Email;
import lombok.NoArgsConstructor;
import org.jsoup.Jsoup;

@NoArgsConstructor
public class HtmlTagsCountClassifier extends AverageBasedClassifier {
    private final static int minHtmlTagLength = 3;
    // The <a> or <p> or any one-letter tag is shortest possible

    private final static double basicHtmlTagsCount = 4d;
    // Jsoup always adds basic html tags by himself if they are not present
    // and count them too. These 4 tags:
    // <html>
    //   <head></head>
    //   <body>
    //     the content
    //   </body>
    // </html>
    // So I decided to exlude them at all even they are present,
    // because I don't know how to track them.

    @Override
    double calculateParameterValue(Email email) {
        if (email.getHtmlContent().length() < minHtmlTagLength) return 0d;
        final double count = Jsoup.parse(email.getHtmlContent()).getAllElements().size();
        return count <= basicHtmlTagsCount ? 0d : count - basicHtmlTagsCount;
    }
}
