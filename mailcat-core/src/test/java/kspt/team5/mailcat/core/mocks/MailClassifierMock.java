package kspt.team5.mailcat.core.mocks;

import com.google.common.collect.Lists;
import kspt.team5.mailcat.core.classification.MailClassifier;
import kspt.team5.mailcat.core.entities.Category;
import kspt.team5.mailcat.core.entities.Email;
import lombok.EqualsAndHashCode;
import lombok.Value;

import java.util.List;

@Value
@EqualsAndHashCode(callSuper = false)
public class MailClassifierMock extends MailClassifier {
    private int intField = 42;

    private String stringField = "Hello world!";

    private List<Category> categoriesList = Lists.newArrayList(new Category("Good"), new Category("Bad"));

    @Override
    public Category classify(Email email) {
        return null;
    }

    @Override
    public void teach(Email email, Category category) {

    }
}
