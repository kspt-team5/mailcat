package kspt.team5.mailcat.core.interactors;

import kspt.team5.mailcat.core.boundaries.MailService;
import kspt.team5.mailcat.core.classification.MailClassifier;
import kspt.team5.mailcat.core.entities.CategorizedEmailsBuffer;
import kspt.team5.mailcat.core.entities.Category;
import kspt.team5.mailcat.core.entities.Email;
import kspt.team5.mailcat.core.entities.Folder;
import kspt.team5.mailcat.core.events.IncomingMessageEvent;
import kspt.team5.mailcat.core.events.MovedMessageEvent;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class MailControllerTest {
    private final MailClassifier mockedMailClassifier = mock(MailClassifier.class);

    private final MailService mockedMailService = mock(MailService.class);

    private final MailController mailController = new MailController(mockedMailClassifier, mockedMailService);

    private final Email someEmail = new Email("0", "", "", "","", "");

    @Test
    public void testHandleIncomingMessageEvent_NonnullClassifierReturn() {
        final IncomingMessageEvent event = new IncomingMessageEvent(someEmail);
        final Category targetCategory = new Category("somecat");
        when(mockedMailClassifier.classify(any())).thenReturn(targetCategory);
        mailController.handleIncomingMessageEvent(event);

        verify(mockedMailClassifier).classify(someEmail);
        verify(mockedMailService).moveEmail(someEmail, Folder.fromCategory(targetCategory));
    }

    @Test
    public void testHandleIncomingMessageEvent_NullClassifierReturn() {
        final IncomingMessageEvent event = new IncomingMessageEvent(someEmail);
        when(mockedMailClassifier.classify(any())).thenReturn(null);

        mailController.handleIncomingMessageEvent(event);

        verify(mockedMailClassifier).classify(someEmail);
        verify(mockedMailService, never()).moveEmail(any(), any());
    }

    @Test
    public void testHandleMovedMessageEvent() {
        final Category targetCategory = new Category("somecat");
        final MovedMessageEvent event = new MovedMessageEvent(someEmail, targetCategory);

        mailController.handleMovedMessageEvent(event);

        verify(mockedMailClassifier).teach(someEmail, targetCategory);
    }

    @Test
    public void testpushCategorizedEmailsBuffer(){
        final IncomingMessageEvent event = new IncomingMessageEvent(someEmail);
        final Category targetCategory = new Category("somecat");

        when(mockedMailClassifier.classify(any())).thenReturn(targetCategory);
        mailController.handleIncomingMessageEvent(event);

        assertThat(CategorizedEmailsBuffer.getInstance().getAll().get(someEmail)).isEqualTo(targetCategory);
    }
}
