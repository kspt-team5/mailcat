package kspt.team5.mailcat.core.interactors;

import kspt.team5.mailcat.core.CannotLoginException;
import kspt.team5.mailcat.core.boundaries.MailService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;

@Slf4j
public class AuthControllerTest {
    private final String host = "imap.fff.ff";
    private final int port = 993;
    private final String email = "asdf@fff.ff";
    private final String password = "passwd";

    @Test
    public void testStartMailService() {
        final MailService mockedMailService = mock(MailService.class);
        final AuthController authController = new AuthController(mockedMailService);

        authController.startMailService(host, port, email, password);

        assertThat(authController.isServiceStarted()).isTrue();
    }

    @Test
    public void testStopMailService() {
        final MailService mockedMailService = mock(MailService.class);
        final AuthController authController = new AuthController(mockedMailService);

        authController.startMailService(host, port, email, password);
        authController.stopMailService();

        assertThat(authController.isServiceStarted()).isFalse();
    }

    @Test(expected = CannotLoginException.class)
    public void testStartMailService_ShouldFail() {
        final MailService mockedMailService = mock(MailService.class);
        final AuthController authController = new AuthController(mockedMailService);

        doThrow(CannotLoginException.class).when(mockedMailService).connect(any(), anyInt(), any(), any());
        authController.startMailService(host, port, email, password);
    }
}
