package kspt.team5.mailcat.core.classification;

import com.google.common.collect.Range;
import org.assertj.core.data.Percentage;
import org.junit.Before;
import org.junit.Test;

import java.util.Random;
import java.util.stream.DoubleStream;

import static org.assertj.core.api.Assertions.assertThat;

public class StatisticalDescriptionTest {
    private static final Range<Double> ZERO_RANGE = Range.closed(0.0, 0.0);

    private static final double CONFIDENCE_PROBABILITY = 0.90;

    private static final double PROXIMITY_PERCENTAGE = 100.0 * (1.0 + CONFIDENCE_PROBABILITY) / 2;

    private StatisticalDescription stat = new StatisticalDescription();

    private Random rng = new Random(42);

    @Before
    public void setUp() {
        stat = new StatisticalDescription();
    }

    @Test
    public void testInitialState() {
        assertThat(stat.getConfidenceInterval()).isEqualTo(ZERO_RANGE);
    }

    @Test
    public void testOneValue() {
        final double val = 42.0;

        stat.update(val);

        assertThat(stat.getAverage()).isEqualTo(val);
        assertThat(stat.getStd()).isEqualTo(0.0);
        assertThat(stat.getConfidenceInterval()).isEqualTo(Range.closed(val, val));
    }

    @Test
    public void testOneThousandValues_Uniform() {
        final double uniformMean = 0.5;
        final double uniformVariance = 1.0 / 12.0;

        rng.doubles(1000).forEach(val -> stat.update(val));

        assertThat(stat.getAverage()).isCloseTo(uniformMean, Percentage.withPercentage(PROXIMITY_PERCENTAGE));
        assertThat(stat.getStd()).isCloseTo(Math.sqrt(uniformVariance), Percentage.withPercentage(PROXIMITY_PERCENTAGE));
        assertThat(stat.getConfidenceInterval().contains(stat.getAverage())).isTrue();
    }

    @Test
    public void testOneThousandValues_Gaussian() {
        final double mean = 0.0;
        final double std = 1.0;

        DoubleStream.generate(() -> rng.nextGaussian()).limit(1000).forEach(val -> stat.update(val));

        assertThat(stat.getStd()).isCloseTo(std, Percentage.withPercentage(PROXIMITY_PERCENTAGE));
        assertThat(stat.getConfidenceInterval().upperEndpoint()).isLessThan(mean + 3*std);
        assertThat(stat.getConfidenceInterval().lowerEndpoint()).isGreaterThan(mean - 3*std);
    }
}
