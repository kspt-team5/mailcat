package kspt.team5.mailcat.core.delivery;

import kspt.team5.mailcat.core.boundaries.MailService;
import kspt.team5.mailcat.core.entities.Email;
import kspt.team5.mailcat.core.entities.Folder;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Set;

@Slf4j
public class IMAP4ServiceTest {
    final MailService mailService = new IMAP4MailService();

    @Before
    public void connect() {
        final String host = "imap.gmail.com";
        final int port = 993;
        final String user = "team5kspt@gmail.com";
        final String password = "team5password";
        mailService.connect(host, port, user, password);
    }
    @After
    public void disconnect() {
        mailService.disconnect();
    }

    @Ignore
    @Test
    public void testGetFolders() {
        Set<Folder> folders =  mailService.getFolders();

        for (Folder f : folders)
            log.info(f.toString());
    }

    @Ignore
    @Test
    public void testMoveMessages() {
        Set<Email> emails =  mailService.getMessages(new Folder("moveFrom"));
        emails.forEach(email -> {
            mailService.moveEmail(email, new Folder("moveTo1"));
        });
    }

    @Ignore
    @Test
    public void testGetEmails() {
        Set<Email> emails =  mailService.getMessages(new Folder("moveFrom"));
        int i = 0;
        for (Email msg : emails) {
            log.info(msg.toString());
            ++i;
//            if (i == 3) break;
        }

    }

}
