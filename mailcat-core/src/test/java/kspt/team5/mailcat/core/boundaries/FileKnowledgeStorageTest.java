package kspt.team5.mailcat.core.boundaries;

import com.google.common.io.Resources;
import kspt.team5.mailcat.core.classification.MailClassifier;
import kspt.team5.mailcat.core.delivery.FileKnowledgeStorage;
import kspt.team5.mailcat.core.mocks.MailClassifierMock;
import org.assertj.core.util.Files;
import org.junit.Test;

import java.io.File;
import java.nio.charset.Charset;

import static org.assertj.core.api.Assertions.assertThat;

public class FileKnowledgeStorageTest {
    private final String FIXTURE = Files.contentOf(
            new File(Resources.getResource("mail-classifier-fixture.json").getFile()),
            Charset.defaultCharset());

    private final File storageFile = Files.newTemporaryFile();

    private final KnowledgeStorage storage = new FileKnowledgeStorage(storageFile);

    @Test
    public void testStore() {
        final MailClassifier classifierMock = new MailClassifierMock();

        storage.store(classifierMock);

        assertThat(Files.contentOf(storageFile, Charset.defaultCharset())).isEqualTo(FIXTURE);
    }

    @Test
    public void testLoad() {
        final MailClassifier actualClassifier = new MailClassifierMock();
        storage.store(actualClassifier);

        final MailClassifier loadedClassifier = storage.load(MailClassifierMock.class);

        assertThat(loadedClassifier).isEqualTo(actualClassifier);
    }

    @Test
    public void testLoadNonExistentFile() {
        final MailClassifier loadedClassifier = storage.load(MailClassifierMock.class);

        assertThat(loadedClassifier).isEqualTo(new MailClassifierMock());
    }
}
