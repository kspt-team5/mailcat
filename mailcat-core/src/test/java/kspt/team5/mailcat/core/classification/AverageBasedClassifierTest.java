package kspt.team5.mailcat.core.classification;

import com.google.common.collect.Range;
import kspt.team5.mailcat.core.entities.Category;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AverageBasedClassifierTest {
    private final AverageBasedClassifier abc = mock(AverageBasedClassifier.class);

    private final Map<Category, StatisticalDescription> someStatistics = new HashMap<>();

    private final Category categoryA = new Category("A");

    private final Category categoryB = new Category("B");

    private final Category categoryC = new Category("C");

    @Before
    public void setUp() {
        someStatistics.put(categoryA, getMockedStatistics(2.34, 2.0));
        someStatistics.put(categoryB, getMockedStatistics(15.7, 0.5));
        someStatistics.put(categoryC, getMockedStatistics(7.0, 5.0));
        when(abc.getCategoryToStatistics()).thenReturn(someStatistics);
    }

    private static StatisticalDescription getMockedStatistics(double avg, double intervalRadius) {
        final StatisticalDescription stat = mock(StatisticalDescription.class);
        when(stat.getAverage()).thenReturn(avg);
        when(stat.getConfidenceInterval()).thenReturn(Range.closed(avg - intervalRadius, avg + intervalRadius));
        return stat;
    }

    @Test
    public void testFindCategoryWithClosestParamValue_EmptyKnowledgeBase() {
        someStatistics.clear();

        final Category category = abc.findCategoryWithClosestParamValue(3.0);

        assertThat(category).isNull();
    }

    @Test
    public void testFindCategoryWithClosestParamValue_CategoryA() {
        final Category category = abc.findCategoryWithClosestParamValue(3.0);

        assertThat(category).isEqualTo(categoryA);
    }

    @Test
    public void testFindCategoryWithClosestParamValue_CategoryB() {
        final Category category = abc.findCategoryWithClosestParamValue(14.333);

        assertThat(category).isEqualTo(categoryB);
    }

    @Test
    public void testFindCategoryWithClosestParamValue_NewCloserCategory() {
        final Category categoryD = new Category("D");
        someStatistics.put(categoryD, StatisticalDescription.beginWith(14.30));

        final Category category = abc.findCategoryWithClosestParamValue(14.333);

        assertThat(category).isEqualTo(categoryD);
    }

    @Test
    public void testFindCategoryWithClosestParamValue_CategoryC_ExactMatch() {
        final Category category = abc.findCategoryWithClosestParamValue(7.0);

        assertThat(category).isEqualTo(categoryC);
    }

    @Test
    public void testFindCategoryByConfidenceIntervalHit_EmptyKnowledgeBase() {
        someStatistics.clear();

        final Category category = abc.findCategoryByConfidenceIntervalHit(3.0);

        assertThat(category).isNull();
    }

    @Test
    public void testFindCategoryByConfidenceIntervalHit_CategoryA() {
        final Category category = abc.findCategoryByConfidenceIntervalHit(1.0);

        assertThat(category).isEqualTo(categoryA);
    }

    @Test
    public void testFindCategoryByConfidenceIntervalHit_NotCategoryB() {
        final Category category = abc.findCategoryByConfidenceIntervalHit(14.333);

        assertThat(category).isNull();
    }

    @Test
    public void testFindCategoryByConfidenceIntervalHit_CategoryB() {
        final Category category = abc.findCategoryByConfidenceIntervalHit(15.233);

        assertThat(category).isEqualTo(categoryB);
    }

    @Test
    public void testFindCategoryByConfidenceIntervalHit_NewCloserCategory() {
        final Category categoryD = new Category("D");
        someStatistics.put(categoryD, getMockedStatistics(14.320, 10.0));

        final Category category = abc.findCategoryByConfidenceIntervalHit(14.333);

        assertThat(category).isEqualTo(categoryD);
    }

    @Test
    public void testFindCategoryByConfidenceIntervalHit_CategoryC_ExactMatch() {
        final Category category = abc.findCategoryByConfidenceIntervalHit(7.0);

        assertThat(category).isEqualTo(categoryC);
    }

    @Test
    public void testFindCategoryByConfidenceIntervalHit_Outlier() {
        final Category category = abc.findCategoryByConfidenceIntervalHit(107.0);

        assertThat(category).isNull();
    }

    @Test
    public void testFindCategoryByConfidenceIntervalHit_Overlap() {
        final Category category = abc.findCategoryByConfidenceIntervalHit(3.0);

        assertThat(category).isNull();
    }
}
