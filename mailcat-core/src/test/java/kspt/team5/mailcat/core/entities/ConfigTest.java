package kspt.team5.mailcat.core.entities;

import com.google.common.collect.Sets;
import org.junit.Test;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class ConfigTest {
    private final String email = "lol@ro.fl";

    private final String password = "qwerty123";

    private final Integer size = 50;

    private final String host = "imap.ro.fl";

    private final int port = 993;

    private final Set<Category> categories =
            Sets.newHashSet(new Category("Рассылки"), new Category("Путешествия"));

    private final Config config = new Config(email, password, categories, size, host, port);

    @Test
    public void testConstructor() {
        assertThat(config).isNotNull();
    }

    @Test
    public void testGetEmail() {
        assertThat(config.getEmail()).isEqualTo(email);
    }

    @Test
    public void testGetPassword() {
        assertThat(config.getPassword()).isEqualTo(password);
    }

    @Test
    public void testGetCategoriesList() {
        assertThat(config.getCategories()).containsSequence(categories);
    }

    @Test
    public void testGetLearningThreshold() {
        assertThat(config.getLearningThreshold()).isEqualTo(size);
    }

    @Test
    public void testGetHost() {
        assertThat(config.getHost()).isEqualTo(host);
    }

    @Test
    public void testGetPort() {
        assertThat(config.getPort()).isEqualTo(port);
    }
}
