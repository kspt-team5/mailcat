package kspt.team5.mailcat.core.entities;

import lombok.Value;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

@Value
public class EmailTest {

    private final Email email = new Email("1", "some subject", "some Plain text",
            "htmlContent", "me", "they");

    @Test
    public void testGetId() {
        assertThat(email.getId()).isEqualTo("1");
    }

    @Test
    public void testGetSubject() {
        assertThat(email.getSubject()).isEqualTo("some subject");
    }

    @Test
    public void testGetPlainContent() {
        assertThat(email.getPlainContent()).isEqualTo("some Plain text");
    }

    @Test
    public void testGetHtmlContent() {
        assertThat(email.getHtmlContent()).isEqualTo("htmlContent");
    }

    @Test
    public void testGetRecipient() {
        assertThat(email.getRecipient()).isEqualTo("me");
    }

    @Test
    public void testGetSender() {
        assertThat(email.getSender()).isEqualTo("they");
    }
}


