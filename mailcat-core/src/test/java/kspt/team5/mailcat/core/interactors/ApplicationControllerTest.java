package kspt.team5.mailcat.core.interactors;

import com.google.common.io.Resources;
import kspt.team5.mailcat.core.boundaries.MailService;
import kspt.team5.mailcat.core.delivery.FileKnowledgeStorage;
import kspt.team5.mailcat.core.delivery.TypesafeConfigLoader;
import kspt.team5.mailcat.core.entities.Config;
import kspt.team5.mailcat.core.entities.Folder;
import kspt.team5.mailcat.core.utilities.ApplicationControllerProvider;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ApplicationControllerTest {
    private static final int STARTUP_TIME_MS = 400;

    private static final int SHUTDOWN_TIME_MS = 200;

    private ApplicationController appController;

    private MailService mockedMailService = mock(MailService.class);

    @Before
    public void create() {
        when(mockedMailService.getFolders()).thenReturn(getValidConfig().getCategories()
                .stream().map(Folder::fromCategory).collect(Collectors.toSet()));
        appController = ApplicationControllerProvider.builder()
                .mailService(mockedMailService)
                .knowledgeStorage(mock(FileKnowledgeStorage.class))
                .build().get();
    }

    @Test
    public void testRun() throws InterruptedException {
        runApplicationControllerInSeparateThread();
        waitForStartup();
        assertThat(appController.isRun()).isTrue();
    }

    @Test
    public void testStop() throws InterruptedException {
        runApplicationControllerInSeparateThread();
        waitForStartup();
        appController.stop();
        waitForShutdown();
        assertThat(appController.isRun()).isFalse();
    }

    private void runApplicationControllerInSeparateThread() {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> appController.run(getValidConfig()));
    }

    @After
    public void stop() {
        if (appController.isRun()) {
            appController.stop();
        }
    }

    private static Config getValidConfig() {
        return new TypesafeConfigLoader(
                new File(Resources.getResource("application.conf-valid").getFile())
        ).getConfig();
    }

    private void waitForStartup() throws InterruptedException {
        Thread.sleep(STARTUP_TIME_MS);
    }

    private void waitForShutdown() throws InterruptedException {
        Thread.sleep(SHUTDOWN_TIME_MS);
    }
}
