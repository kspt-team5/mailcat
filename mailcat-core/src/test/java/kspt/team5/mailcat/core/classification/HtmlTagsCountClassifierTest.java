package kspt.team5.mailcat.core.classification;

import kspt.team5.mailcat.core.entities.Email;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class HtmlTagsCountClassifierTest {

    private final HtmlTagsCountClassifier htmlTagsCountClassifier = new HtmlTagsCountClassifier();

    @Test
    public void calculateParameterValue_NoContent() {
        Email email = new Email("1", "Subject", "", "","Recipient", "Sender");
        assertThat(htmlTagsCountClassifier.calculateParameterValue(email)).isEqualTo(0d);
    }

    @Test
    public void calculateParameterValue_NoHtmlContent() {
        Email email = new Email("1", "Subject", "", "There is no html.","Recipient", "Sender");
        assertThat(htmlTagsCountClassifier.calculateParameterValue(email)).isEqualTo(0d);
    }

    @Test
    public void calculateParameterValue_NormalHtmlContent1() {
        String content = "<a href=\"google.com\">a</a>" +
                "<a target=\"_blank\" href=\"google.com\">a</a>" +
                "<a style=\"text-decoration: none;\" href=\"google.com\">a</a>" +
                "<a style=\"text-decoration: none;\" target=\"_blank\" href=\"google.com\">a</a>" +
                "<a href=\"google.com\" style=\"text-decoration: none;\">a</a>";
        Email email = new Email("1", "Subject", "", content, "Recipient", "Sender");
        assertThat(htmlTagsCountClassifier.calculateParameterValue(email)).isEqualTo(5d);
    }

    @Test
    public void calculateParameterValue_NormalHtmlContent2() {
        String content = "<html>" +
                "<head></head>" +
                "<body>" +
                "<a style=\"text-decoration: none;\" target=\"_blank\" href=\"google.com\">a</a>" +
                "</body></html>";
        Email email = new Email("1", "Subject", "", content, "Recipient", "Sender");
        assertThat(htmlTagsCountClassifier.calculateParameterValue(email)).isEqualTo(1d);
    }

    @Test
    public void calculateParameterValue_BrokenHtmlContent1() {
    String content = "<>";
        Email email = new Email("1", "Subject", "", content, "Recipient", "Sender");
        assertThat(htmlTagsCountClassifier.calculateParameterValue(email)).isEqualTo(0d);
    }

    @Test
    public void calculateParameterValue_BrokenHtmlContent2() {
        String content = "a href=\"google.com\">";
        Email email = new Email("1", "Subject", "", content, "Recipient", "Sender");
        assertThat(htmlTagsCountClassifier.calculateParameterValue(email)).isEqualTo(0d);
    }

    @Test
    public void calculateParameterValue_BrokenHtmlContent3() {
        String content = "<a href=\"https://www.youtube.com/watch?v=tauYnVE6ykU\"" +
                "<a href=\"google.com\">Wazzup</a>";
        Email email = new Email("1", "Subject", "", content, "Recipient", "Sender");
        assertThat(htmlTagsCountClassifier.calculateParameterValue(email)).isEqualTo(1d);
    }

    @Test
    public void calculateParameterValue_BrokenHtmlContent4() {
        String content = "<a href=\"https://www.youtube.com/watch?v=tauYnVE6ykU\">" +
                "<a href=\"google.com\">Wazzup</a></a></a></p>";
        Email email = new Email("1", "Subject", "", content, "Recipient", "Sender");
        assertThat(htmlTagsCountClassifier.calculateParameterValue(email)).isEqualTo(3d);
    }
}