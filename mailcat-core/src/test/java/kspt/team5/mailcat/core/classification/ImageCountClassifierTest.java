package kspt.team5.mailcat.core.classification;

import kspt.team5.mailcat.core.entities.Email;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ImageCountClassifierTest {

    private final ImageCountClassifier imageCountClassifier = new ImageCountClassifier();

    @Test
    public void calculateParameterValue_NoContent() {
        Email email = new Email("1", "Subject", "","", "Recipient", "Sender");
        assertThat(imageCountClassifier.calculateParameterValue(email)).isEqualTo(0d);
    }

    @Test
    public void calculateParameterValue_NoHtmlContent() {
        Email email = new Email("1", "Subject", "","There is no html.", "Recipient", "Sender");
        assertThat(imageCountClassifier.calculateParameterValue(email)).isEqualTo(0d);
    }

    @Test
    public void calculateParameterValue_NormalHtmlContent1() {
        String content = "<img aria-hidden=\"true\" src=\"https://bitbucket.org/kspt-team5/mailcat/avatar/32/\" class=\"cbFzAg\">" +
                "<img aria-hidden=\"true\" src=\"https://bitbucket.org/kspt-team5/mailcat/avatar/32/\" class=\"cbFzAg\">" +
                "<img aria-hidden=\"true\" src=\"https://bitbucket.org/kspt-team5/mailcat/avatar/32/\" class=\"cbFzAg\">";
        Email email = new Email("1", "Subject", "",content, "Recipient", "Sender");
        assertThat(imageCountClassifier.calculateParameterValue(email)).isEqualTo(3d);
    }

    @Test
    public void calculateParameterValue_BrokenHtmlContent1() {
        String content = "<img>";
        Email email = new Email("1", "Subject", "", content, "Recipient", "Sender");
        assertThat(imageCountClassifier.calculateParameterValue(email)).isEqualTo(0d);
    }

    @Test
    public void calculateParameterValue_BrokenHtmlContent2() {
        String content = "<img aria-hidden=\"true\" src=\"https://bitbucket.org/kspt-team5/mailcat/avatar/32/\" class=\"cbFzAg\"";
        Email email = new Email("1", "Subject", "", content, "Recipient", "Sender");
        assertThat(imageCountClassifier.calculateParameterValue(email)).isEqualTo(0d);
    }

    @Test
    public void calculateParameterValue_BrokenHtmlContent3() {
        String content = "img aria-hidden=\"true\" src=\"https://bitbucket.org/kspt-team5/mailcat/avatar/32/\" class=\"cbFzAg\">";
        Email email = new Email("1", "Subject", "", content, "Recipient", "Sender");
        assertThat(imageCountClassifier.calculateParameterValue(email)).isEqualTo(0d);
    }
}