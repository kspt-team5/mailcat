package kspt.team5.mailcat.core.entities;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CategoryTest {
    private final String name = "Рассылки";
    private final Category category = new Category(name);

    @Test
    public void testConstructor() {
        assertThat(category).isNotNull();
    }

    @Test
    public void testGetName() {
        assertThat(category.getName()).isEqualTo(name);
    }
}
