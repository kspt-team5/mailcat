package kspt.team5.mailcat.core.entities;

import kspt.team5.mailcat.core.classification.SimpleMailClassifier;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SimpleMailClassifierTest {
    private final SimpleMailClassifier classifier = new SimpleMailClassifier();
    private Email email = new Email("10", "subject","text/plain","","me", "sender");
    private  Category category = new Category("Category1");

    @Test
    public void testClassify(){
        classifier.getKnowledgeBase().put(email.getSender(),category);
        assertThat(classifier.classify(email)).isEqualTo(category);
    }

    @Test
    public void testTeach(){
        classifier.teach(email,category);
        assertThat(classifier.getKnowledgeBase().get(email.getSender())).isEqualTo(category);
    }

    @Test
    public void testClassifyAndTeach(){
        classifier.teach(email,category);
        assertThat(classifier.classify(email)).isEqualTo(category);
    }
}
