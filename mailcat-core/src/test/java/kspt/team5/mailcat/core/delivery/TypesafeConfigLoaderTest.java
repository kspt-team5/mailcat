package kspt.team5.mailcat.core.delivery;

import com.google.common.collect.Sets;
import com.google.common.io.Resources;
import kspt.team5.mailcat.core.entities.Category;
import kspt.team5.mailcat.core.entities.Config;
import org.junit.Test;

import java.io.File;
import java.util.Set;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class TypesafeConfigLoaderTest {
    @Test
    public void testGetConfig_ValidFile() {
        final TypesafeConfigLoader loader = new TypesafeConfigLoader(getConfigFile("application.conf-valid"));
        final Config config = loader.getConfig();

        Config expectedConfig = getExpectedConfig();

        assertThat(config).isEqualTo(expectedConfig);
    }

    @Test
    public void testGetConfig_DuplicateCategories() {
        final TypesafeConfigLoader loader = new TypesafeConfigLoader(getConfigFile("application.conf-dupcat"));
        final Config config = loader.getConfig();

        Config expectedConfig = getExpectedConfig();

        assertThat(config).isEqualTo(expectedConfig);
    }

    @Test(expected = com.typesafe.config.ConfigException.class)
    public void testGetConfig_InvalidFile() {
        final TypesafeConfigLoader loader = new TypesafeConfigLoader(getConfigFile("application.conf-invalid"));
        loader.getConfig();
    }

    private Config getExpectedConfig() {
        final String email = "test@example.com";
        final String password = "qwerty123";
        final Set<Category> cats =
                Sets.newHashSet(new Category("one"), new Category("two"), new Category("three"));
        final int learningThreshold = 50;
        final String host = "imap.example.com";
        final int port = 993;
        return new Config(email, password, cats, learningThreshold, host, port);
    }

    private static File getConfigFile(final String name) {
        return new File(Resources.getResource(name).getFile());
    }
}
