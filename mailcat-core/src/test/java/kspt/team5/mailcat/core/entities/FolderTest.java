package kspt.team5.mailcat.core.entities;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class FolderTest {
    private final String name = "Рассылки";
    private final Folder folder = new Folder(name);

    @Test
    public void testConstructor() {
        assertThat(folder).isNotNull();
    }

    @Test
    public void testGetName() {
        assertThat(folder.getName()).isEqualTo(name);
    }
}