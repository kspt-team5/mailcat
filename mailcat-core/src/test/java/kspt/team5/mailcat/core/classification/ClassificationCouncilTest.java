package kspt.team5.mailcat.core.classification;

import com.google.common.base.Preconditions;
import kspt.team5.mailcat.core.entities.Category;
import kspt.team5.mailcat.core.entities.Email;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class ClassificationCouncilTest {
    private final static int MAX_MEMBERS = 8;

    private final List<MailClassifier> classifiers = getClassifiers(MAX_MEMBERS);

    private final Email email = Email.empty();
    
    private int currentClassifierIdx;

    private static List<MailClassifier> getClassifiers(final int number) {
        return IntStream.rangeClosed(1, number)
                .mapToObj(num -> mock(MailClassifier.class, "Classifier " + num))
                .collect(Collectors.toList());
    }
    
    private ClassificationCouncil council() {
        return getCouncilOf(currentClassifierIdx, classifiers);
    }

    private static ClassificationCouncil getCouncilOf(final int number, final List<MailClassifier> classifiers) {
        Preconditions.checkArgument(number > 0 && number <= MAX_MEMBERS);
        final ClassificationCouncil cc = new ClassificationCouncil();
        for (int i = 0; i < number; i++) {
            cc.registerMember(classifiers.get(i));
        }
        return cc;
    }
    
    @Before
    public void setUp() {
        currentClassifierIdx = 0;
    }

    private void voteFor(String categoryName) {
        when(classifiers.get(currentClassifierIdx).classify(any())).thenReturn(new Category(categoryName));
        currentClassifierIdx++;
    }

    private void uncertain() {
        when(classifiers.get(currentClassifierIdx).classify(any())).thenReturn(null);
        currentClassifierIdx++;
    }

    @Test
    public void singleClassifierDecision() {
        voteFor("A");
        final Category expectedResult = new Category("A");

        final Category result = council().classify(email);

        assertThat(result).isEqualTo(expectedResult);
    }

    @Test
    public void simpleDecision() {
        voteFor("A");
        voteFor("B");
        voteFor("B");
        voteFor("B");
        final Category expectedResult = new Category("B");

        final Category result = council().classify(email);

        assertThat(result).isEqualTo(expectedResult);
    }

    @Test
    public void unanimousDecision() {
        voteFor("A");
        voteFor("A");
        voteFor("A");
        voteFor("A");
        final Category expectedResult = new Category("A");

        final Category result = council().classify(email);

        assertThat(result).isEqualTo(expectedResult);
    }

    @Test
    public void equalDecisions() {
        voteFor("A");
        voteFor("A");
        voteFor("B");
        voteFor("B");

        final Category result = council().classify(email);

        assertThat(result).isNull();
    }

    @Test
    public void noExactDecision() {
        voteFor("A");
        voteFor("B");
        voteFor("C");
        voteFor("D");

        final Category result = council().classify(email);

        assertThat(result).isNull();
    }

    @Test
    public void simpleDecision_OneUncertain() {
        uncertain();
        voteFor("A");
        voteFor("B");
        voteFor("B");
        final Category expectedResult = new Category("B");

        final Category result = council().classify(email);

        assertThat(result).isEqualTo(expectedResult);
    }

    @Test
    public void unanimousDecision_OneUncertain() {
        uncertain();
        voteFor("A");
        voteFor("A");
        voteFor("A");
        final Category expectedResult = new Category("A");

        final Category result = council().classify(email);

        assertThat(result).isEqualTo(expectedResult);
    }

    @Test
    public void equalDecisions_OneUncertain() {
        uncertain();
        voteFor("A");
        voteFor("A");
        voteFor("B");
        voteFor("B");

        final Category result = council().classify(email);

        assertThat(result).isNull();
    }

    @Test
    public void noExactDecision_OneUncertain() {
        voteFor("A");
        voteFor("B");
        voteFor("C");
        voteFor("D");
        uncertain();

        final Category result = council().classify(email);

        assertThat(result).isNull();
    }

    @Test
    public void simpleDecision_HalfUncertain() {
        uncertain();
        uncertain();
        uncertain();
        voteFor("A");
        voteFor("A");
        voteFor("B");
        final Category expectedResult = new Category("A");

        final Category result = council().classify(email);

        assertThat(result).isEqualTo(expectedResult);
    }

    @Test
    public void unanimousDecision_HalfUncertain() {
        uncertain();
        uncertain();
        voteFor("B");
        voteFor("B");
        final Category expectedResult = new Category("B");

        final Category result = council().classify(email);

        assertThat(result).isEqualTo(expectedResult);
    }


    @Test
    public void equalDecisions_HalfUncertain() {
        uncertain();
        uncertain();
        uncertain();
        uncertain();
        voteFor("A");
        voteFor("A");
        voteFor("C");
        voteFor("C");

        final Category result = council().classify(email);

        assertThat(result).isNull();
    }

    @Test
    public void noExactDecision_HalfUncertain() {
        uncertain();
        uncertain();
        uncertain();
        voteFor("A");
        voteFor("B");
        voteFor("C");

        final Category result = council().classify(email);

        assertThat(result).isNull();
    }

    @Test
    public void mixedDecision() {
        uncertain();
        voteFor("A");
        voteFor("B");
        uncertain();
        voteFor("C");
        voteFor("A");
        voteFor("C");
        voteFor("C");
        final Category expectedResult = new Category("C");

        final Category result = council().classify(email);

        assertThat(result).isEqualTo(expectedResult);
    }

    @Test
    public void noDecision_MajorityUncertain() {
        final int majority = (int) Math.floor(MAX_MEMBERS * MajoritySolver.MAJORITY_COEFF) + 1;
        IntStream.rangeClosed(1, majority).forEach(i -> uncertain());
        IntStream.rangeClosed(1, MAX_MEMBERS - majority).forEach(i -> voteFor("A"));

        final Category result = council().classify(email);

        assertThat(result).isNull();
    }

    @Test
    public void noDecision_AllUncertain() {
        uncertain();
        uncertain();
        uncertain();

        final Category result = council().classify(email);

        assertThat(result).isNull();
    }

    @Test
    public void testTeach() {
        final ClassificationCouncil council = getCouncilOf(3, classifiers);
        final Email email = Email.empty();
        final Category category = new Category("test");

        council.teach(email, category);

        final List<MailClassifier> members = council.getMembers();
        members.forEach(member -> verify(member).teach(email, category));
    }
}