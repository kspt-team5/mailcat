package kspt.team5.mailcat.core.entities;

import org.junit.Before;
import org.junit.Test;

import java.util.LinkedHashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class CategorizedEmailsBufferTest {

    private static int flagbuf = 0;

    private CategorizedEmailsBuffer categorizedEmailsBuffer = CategorizedEmailsBuffer.getInstance();

    private Map<Email, Category> CreateMap(int num) {
        final Map<Email, Category> storage = new LinkedHashMap<>();
        if(num == 0) return storage;
        if(num == 10) {for (int i = 0; i < num; i++) storage.put(GetEmail(i),GetCategory(i)); return  storage;}
        for (int i = num - 20; i < num; i++) storage.put(GetEmail(i),GetCategory(i)); return storage;
    }

    private Email GetEmail(int i) {
        return new Email("" + i, "" + i, "" + i, "" + i, "" + i, "" + i);
    }

    private Category GetCategory(int i) {
        return new Category("" + i);
    }

    private void SetEmailCategory(CategorizedEmailsBuffer buf, int num){
        for(int i=0;i<num;i++) buf.push(GetEmail(flagbuf + i), GetCategory(flagbuf + i));
        flagbuf +=10;
    }

    @Before
    public void setUp() {
        categorizedEmailsBuffer.getAll().clear();
        flagbuf=0;
    }


    @Test
    public void getInstanceTest(){
        assertThat(CategorizedEmailsBuffer.getInstance()).isEqualTo(CategorizedEmailsBuffer.getInstance());
    }

    @Test
    public void buf0_getAllTest(){
        SetEmailCategory(categorizedEmailsBuffer, 0);
        assertThat(categorizedEmailsBuffer.getAll()).isEqualTo(CreateMap(0));
    }

    @Test
    public void buf10_getAllTest(){
        SetEmailCategory(categorizedEmailsBuffer, 10);
        assertThat(categorizedEmailsBuffer.getAll()).isEqualTo(CreateMap(10));
    }

    @Test
    public void buf20_getAllTest(){
        SetEmailCategory(categorizedEmailsBuffer, 20);
        assertThat(categorizedEmailsBuffer.getAll()).isEqualTo(CreateMap(20));
    }

    @Test
    public void buf40_getAllTest(){
        SetEmailCategory(categorizedEmailsBuffer, 40);
        assertThat(categorizedEmailsBuffer.getAll()).isEqualTo(CreateMap(40));
    }

    @Test
    public void buf10_Plus_buf0_getAllTest(){
        SetEmailCategory(categorizedEmailsBuffer, 10);
        SetEmailCategory(categorizedEmailsBuffer, 0);
        assertThat(categorizedEmailsBuffer.getAll()).isEqualTo(CreateMap(10));
    }
    @Test
    public void buf10_Plus_buf10_getAllTest(){
        SetEmailCategory(categorizedEmailsBuffer, 10);
        SetEmailCategory(categorizedEmailsBuffer, 10);
        assertThat(categorizedEmailsBuffer.getAll()).isEqualTo(CreateMap(20));
    }
    @Test
    public void buf10_Plus_buf20_getAllTest(){
        SetEmailCategory(categorizedEmailsBuffer, 10);
        SetEmailCategory(categorizedEmailsBuffer, 20);
        assertThat(categorizedEmailsBuffer.getAll()).isEqualTo(CreateMap(30));
    }
    @Test
    public void buf10_Plus_buf40_getAllTest(){
        SetEmailCategory(categorizedEmailsBuffer, 10);
        SetEmailCategory(categorizedEmailsBuffer, 40);
        assertThat(categorizedEmailsBuffer.getAll()).isEqualTo(CreateMap(50));
    }
}