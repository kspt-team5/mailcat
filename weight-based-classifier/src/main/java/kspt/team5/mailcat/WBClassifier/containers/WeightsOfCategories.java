package kspt.team5.mailcat.WBClassifier.containers;

import kspt.team5.mailcat.core.entities.Category;

import java.util.HashMap;
import java.util.Objects;

public class WeightsOfCategories extends HashMap<Category, TermsHashMap> {

    public WeightsOfCategories(CategoriesHashMap map) {
        buildWeightsOfCategoriesAndRemoveMinorWordsMapFrom(map);
    }

    private void buildWeightsOfCategoriesAndRemoveMinorWordsMapFrom(CategoriesHashMap map) {
        map.forEach((category, documents) -> documents.forEach((document, terms) -> {
            terms.forEach((term, weight) -> {
                if (documents.entrySet().stream()
                        .anyMatch(d -> d.getValue().containsKey(term) && !Objects.equals(d.getKey(), document)))
                    if (containsKey(category)) {
                        get(category).add(term, weight);
                    } else {
                        put(category, new TermsHashMap());
                        get(category).add(term, weight);
                    }
            });
        }));
    }
}
