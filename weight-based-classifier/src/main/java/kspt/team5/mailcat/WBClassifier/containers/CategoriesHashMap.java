package kspt.team5.mailcat.WBClassifier.containers;

import kspt.team5.mailcat.core.entities.Category;

import java.util.HashMap;

public class CategoriesHashMap extends HashMap<Category, DocumentsHashMap> {
    public void add(Category category, DocumentsHashMap documents) {
        if (containsKey(category)) {
            documents.forEach((documentId, termsMap) -> {
                get(category).add(documentId, termsMap);
            });
        } else {
            put(category, new DocumentsHashMap());
            add(category, documents);
        }
    }
}
