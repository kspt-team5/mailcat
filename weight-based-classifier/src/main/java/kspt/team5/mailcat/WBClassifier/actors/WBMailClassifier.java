package kspt.team5.mailcat.WBClassifier.actors;

import kspt.team5.mailcat.WBClassifier.containers.CategoriesHashMap;
import kspt.team5.mailcat.WBClassifier.containers.DocumentsHashMap;
import kspt.team5.mailcat.WBClassifier.containers.TermsHashMap;
import kspt.team5.mailcat.WBClassifier.containers.WeightsOfCategories;
import kspt.team5.mailcat.core.classification.MailClassifier;
import kspt.team5.mailcat.core.entities.Category;
import kspt.team5.mailcat.core.entities.Email;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;

public class WBMailClassifier extends MailClassifier {
    @Setter
    private double margin = 0.2;

    @Getter
    private CategoriesHashMap knowledgeBase = new CategoriesHashMap();

    @Override
    public Category classify(Email email) {
        WeightsOfCategories weightsOfCategories = new WeightsOfCategories(knowledgeBase);
        TermsHashMap weightsOfIncomingMsg;
        if (email.getPlainContent().length() > 1)
            weightsOfIncomingMsg = Parser.parseTermsPlain(email.getPlainContent());
        else
            weightsOfIncomingMsg = Parser.parseTermsHtml(email.getHtmlContent());
        HashMap<Category, Double> participationScores =
                calculateCategoryParticipation(weightsOfIncomingMsg, weightsOfCategories);
        return determineResultCategory(participationScores);
    }

    @Override
    public void teach(Email email, Category category) {
        DocumentsHashMap documents = new DocumentsHashMap();
        TermsHashMap terms;
        if (email.getPlainContent().length() > 1)
            terms = Parser.parseTermsPlain(email.getPlainContent());
        else
            terms = Parser.parseTermsHtml(email.getHtmlContent());
        documents.add(email.getId(), terms);
        knowledgeBase.add(category, documents);
    }

    private HashMap<Category, Double> calculateCategoryParticipation
            (TermsHashMap weightsOfIncomingMsg, WeightsOfCategories weightsOfCategories) {
        weightsOfCategories.forEach(((category, weights) -> normalizeWeights(weights)));
        HashMap<Category, Double> result = new HashMap<>();
        weightsOfCategories.forEach((category, terms) -> {
            Double score = weightsOfIncomingMsg.entrySet().stream().mapToDouble(
                    term -> term.getValue() * terms.getOrDefault(term.getKey(), 0d)
            ).sum();
            result.put(category, score);
        });
        return result;
    }

    private Category determineResultCategory(HashMap<Category, Double> scores) {
        Double scoresSum = scores.values().stream().mapToDouble(a -> a).sum();
        HashMap<Category, Double> confidence = new HashMap<>();
        scores.forEach((category, score) -> {
            confidence.put(category, score / scoresSum);
        });
        Category MostAppropCategory = chooseMostAppropCategory(scores);
        Double mostAppropCategoryScore = confidence.getOrDefault(MostAppropCategory, 0d);
        Double nextMostAppropCategoryScore = confidence.getOrDefault(chooseNextMostAppropCategory(scores), 0d);
        if (mostAppropCategoryScore - nextMostAppropCategoryScore > nextMostAppropCategoryScore * margin)
            return MostAppropCategory;
        else
            return null;
    }

    private Category chooseNextMostAppropCategory(HashMap<Category, Double> result) {
        Category mostApprop = chooseMostAppropCategory(result);
        Category next = null;
        Double nextValue = 0d;
        for (Category key : result.keySet()) {
            if (key == mostApprop)
                continue;
            if (result.get(key) > nextValue) {
                nextValue = result.get(key);
                next = key;
            }
        }
        return next;
    }

    private Category chooseMostAppropCategory(HashMap<Category, Double> result) {
        Category mostApprop = null;
        Double mostValue = 0d;
        for (Category key : result.keySet())
            if (result.get(key) > mostValue) {
                mostValue = result.get(key);
                mostApprop = key;
            }
        return mostApprop;
    }

    private void normalizeWeights(TermsHashMap weights) {
        Double sum = weights.values().stream().mapToDouble(a -> a).sum();
        weights.forEach((term, weight) -> {
            weights.put(term, weight / sum);
        });
    }
}
