package kspt.team5.mailcat.WBClassifier.containers;

import java.util.HashMap;

public class TermsHashMap extends HashMap<String, Double> {
    public void add(String term, Double additionalValue) {
        if (containsKey(term)) {
            Double currentValue = get(term);
            put(term, currentValue + additionalValue);
        } else {
            put(term, additionalValue);
        }
    }
}
