package kspt.team5.mailcat.WBClassifier.actors;

import kspt.team5.mailcat.WBClassifier.containers.TermsHashMap;
import org.apache.commons.lang3.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class Parser {
    private static final Pattern NOISEWORDSRU = Pattern.compile("^(-|еще|него|сказать|а|ж|нее|со|без|же|ней|совсем|более|жизнь|нельзя|так|больше|за|нет|такой|будет|зачем|ни|там|будто|здесь|нибудь|тебя|бы|и|никогда|тем|был|из|ним|теперь|была|из-за|них|то|были|или|ничего|тогда|было|им|но|того|быть|иногда|ну|тоже|в|их|о|только|вам|к|об|том|вас|кажется|один|тот|вдруг|как|он|три|ведь|какая|она|тут|во|какой|они|ты|вот|когда|опять|у|впрочем|конечно|от|уж|все|которого|перед|уже|всегда|которые|по|хорошо|всего|кто|под|хоть|всех|куда|после|чего|всю|ли|потом|человек|вы|лучше|потому|чем|г|между|почти|через|где|меня|при|что|говорил|мне|про|чтоб|да|много|раз|чтобы|даже|может|разве|чуть|два|можно|с|эти|для|мой|сам|этого|до|моя|свое|этой|другой|мы|свою|этом|его|на|себе|этот|ее|над|себя|эту|ей|надо|сегодня|я|ему|наконец|сейчас|если|нас|сказал|есть|не|сказала)$");
    private static final Pattern NOISEWORDSEN = Pattern.compile("(a|about|after|all|also|an|another|any|are|as|and|at|be|because|been|before|being|between|but|both|by|came|can|come|could|did|do|teach|even|for|from|further|furthermore|get|got|has|had|he|have|her|here|him|himself|his|how|hi|however|i|if|in|into|is|it|its|indeed|just|like|made|many|me|might|more|moreover|most|much|must|my|never|not|now|of|on|only|other|our|out|or|over|said|same|see|should|since|she|some|still|such|take|than|that|the|their|them|then|there|these|therefore|they|this|those|through|to|too|thus|under|up|very|was|way|we|well|were|what|when|where|which|while|who|will|with|would|you|your|nbsp)$");

    public static TermsHashMap parseTermsPlain(String content) {
        content = stripGarbage(content);
        content = content.toLowerCase().replace('\u0451', '\u0435');
        String[] words = content.split("[^a-zA-Zа-яА-Я]");
        words = clearAndStem(words);
        TermsHashMap terms = new TermsHashMap();
        for (String word : words)
            terms.add(word, 1d);
        return terms;
    }

    public static TermsHashMap parseTermsHtml(String content) {
        content = stripHtmlAndCss(content);
        content = stripGarbage(content);
        content = content.toLowerCase().replace('\u0451', '\u0435');
        String[] words = content.split("[^a-zA-Zа-яА-Я]");
        words = clearAndStem(words);
        TermsHashMap terms = new TermsHashMap();
        for (String word : words)
            terms.add(word, 1d);
        return terms;
    }

    private static String stripGarbage(String content) {
        content = stripURLs(content);
        content = stripDigits(content);
        return content;
    }

    private static String[] clearAndStem(String[] words) {
        words = removeEmptyWords(words);
        words = removeNoiseWords(words);
        words = removeEmptyWords(words);
        words = stemRuEn(words);
        return words;
    }

    private static String[] stemRuEn(String[] words) {
        StemmerEN stemmerEN = new StemmerEN();
        for (int i = 0; i < words.length; ++i) {
            words[i] = StemmerRU.stem(words[i]);
            words[i] = stemmerEN.stem(words[i]);
        }
        return words;
    }

    private static String stripHtmlAndCss(String content) {
        content = Jsoup.clean(content, Whitelist.none());
        return StringEscapeUtils.unescapeHtml4(content);
    }

    private static String stripURLs(String content) {
        String regExpURL = "(((http|ftp|https):\\/\\/)?[\\wа-яА-Я\\-_]+(\\.[\\wа-яА-Я\\-_]+)+([\\wа-яА-Я\\-\\.,@?^=%&amp;:/~\\+#]*[\\wа-яА-Я\\-\\@?^=%&amp;/~\\+#])?)";
        return content.replaceAll(regExpURL, "");
    }

    private static String stripDigits(String content) {
        return content.replaceAll("[0-9]", "");
    }

    private static String[] removeEmptyWords(String[] words) {
        List<String> list = new ArrayList<>();
        for (String word : words) {
            if (word != null && word.length() > 0)
                list.add(word);
        }
        words = list.toArray(new String[list.size()]);
        return words;
    }

    private static String[] removeNoiseWords(String[] words) {
        for (int i = 0; i < words.length; i++) {
            words[i] = NOISEWORDSEN.matcher(words[i]).replaceFirst("");
            words[i] = NOISEWORDSRU.matcher(words[i]).replaceFirst("");
        }
        return words;
    }
}
