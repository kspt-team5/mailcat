package kspt.team5.mailcat.WBClassifier.containers;

import java.util.HashMap;

public class DocumentsHashMap extends HashMap<String, TermsHashMap> {
    public void add(String id, TermsHashMap terms) {
        if (containsKey(id)) {
            terms.forEach((term, value) -> {
                get(id).add(term, value);
            });
        } else {
            put(id, new TermsHashMap());
            add(id, terms);
        }
    }
}
