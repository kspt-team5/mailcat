package kspt.team5.mailcat.WBClassifier;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Resources;
import kspt.team5.mailcat.core.entities.Email;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import static org.assertj.core.api.Assertions.assertThat;

public class GetTestingResource {

    @Test
    public void Test() {
        Double a = 0d;
        for (int i = 0; i < 1000; i++){
            a += 0d;
            assertThat(a).isEqualTo(0d);
        }
    }

    public static String asString(String filename) {
        filename = Resources.getResource(filename).getFile();
        StringBuilder records = new StringBuilder();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(filename));
            String line;
            while ((line = reader.readLine()) != null)
                records.append(" ").append(line);
            reader.close();
            return org.apache.commons.lang3.StringEscapeUtils.unescapeJava(records.toString());
        } catch (Exception e) {
            System.err.format("Exception occurred trying to read '%s'.", filename);
            e.printStackTrace();
            return null;
        }
    }

    public static ArrayList<Email> loadJsonCategorizedEmails(String dir) {
        ArrayList<Email> emails = new ArrayList<>();
        File[] cats = new File(dir).listFiles();
        for (File cat : cats) {
            File[] files = cat.listFiles();
            for (File file : files) {
                emails.add(loadJsonEmail(file, cat.getName()));
            }
        }
        return emails;
    }

    public static Email loadJsonEmail(File file, String category) {
        final ObjectMapper mapper = new ObjectMapper();
        HashMap<String, ArrayList<HashMap<String, String>>> messageJsonFile = new HashMap<>();
        try {
            messageJsonFile = mapper.readValue(file, messageJsonFile.getClass());
        } catch (IOException e) {
            e.printStackTrace();
        }
        String address = messageJsonFile.get("from").get(0).get("email");
        String subject = String.valueOf(messageJsonFile.get("subject"));
        String content = "";
        for (int i = 0; i < messageJsonFile.get("attachments").size(); i++)
            if (messageJsonFile.get("attachments").get(i).get("content_type").equals("text/plain") ||
                    messageJsonFile.get("attachments").get(i).get("content_type").equals("text/html"))
                content += messageJsonFile.get("attachments").get(i).get("content");
        for (int i = 0; i < messageJsonFile.get("parts").size(); i++)
            content += messageJsonFile.get("parts").get(i).get("content");

        return new Email(String.valueOf(content.hashCode()), subject, content, content, category,address);
    }
}
