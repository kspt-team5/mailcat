package kspt.team5.mailcat.WBClassifier.containers;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DocumentsHashMapTest {

    private String id = "1";
    private DocumentsHashMap documents = new DocumentsHashMap();
    private String word = "Слово";
    private TermsHashMap terms = new TermsHashMap();

    @Test
    public void test_BasicHashMapMethods() {
        terms.put(word, 10d);
        documents.put(id, terms);
        assertThat(documents.get(id).get(word)).isEqualTo(10d);
    }

    @Test
    public void test_AddMethod_ActuallyWritesSum() {
        terms.put(word, 1000.1d);
        documents.put(id, terms);
        documents.add(id, terms);
        assertThat(documents.get(id).get(word)).isLessThan   (2000.20000000001d);
        assertThat(documents.get(id).get(word)).isGreaterThan(2000.1999999999d);
    }

    @Test
    public void addMethod_OnEmptyHashPair_ShoudActLikePutMethod() {
        terms.put(word, 10d);
        documents.add(id, terms);
        assertThat(documents.get(id).get(word)).isEqualTo(10d);
    }

    @Test
    public void addMethod_OnExpandingTermHashMap_ShoulAddNewTerms() {
        String word2 = "ааа";
        String word3 = "Предел";
        TermsHashMap expandingTerms = new TermsHashMap();
        terms.put(word, 1000.1d);
        expandingTerms.put(word, 2.1d);
        expandingTerms.put(word2, 1009.1d);
        expandingTerms.put(word3, 0d);
        documents.add(id, terms);
        documents.add(id, expandingTerms);
        assertThat(documents.get(id).get(word)).isLessThan   (1002.20000000001d);
        assertThat(documents.get(id).get(word)).isGreaterThan(1002.1999999999d);
        assertThat(documents.get(id).get(word2)).isEqualTo(1009.1d);
        assertThat(documents.get(id).get(word3)).isEqualTo(0d);
    }
}
