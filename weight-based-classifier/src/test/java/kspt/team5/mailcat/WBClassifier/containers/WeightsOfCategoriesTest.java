package kspt.team5.mailcat.WBClassifier.containers;

import kspt.team5.mailcat.WBClassifier.GetTestingResource;
import kspt.team5.mailcat.WBClassifier.actors.Parser;
import kspt.team5.mailcat.core.entities.Category;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class WeightsOfCategoriesTest {
    private String content = GetTestingResource.asString("content.txt");
    private TermsHashMap terms = Parser.parseTermsHtml(content);
    private DocumentsHashMap documents = new DocumentsHashMap();
    private CategoriesHashMap categories = new CategoriesHashMap();

    @Test
    public void constructorMustReceive_CategoriesHashMap_AsArgument() {
        WeightsOfCategories weightsOfCategories = new WeightsOfCategories(categories);
        assertThat(weightsOfCategories)
                .isNotNull();
    }

    @Test
    public void constructorMustParseAndStore_NoneTermsFrom_CategoriesHashMapWithOnlyOneTermsMap() {
        documents.add("1",terms);
        categories.add(new Category("Cat"), documents);
        WeightsOfCategories weightsOfCategories = new WeightsOfCategories(categories);
        final int[] catched = {0};
        final int[] tries = {0};
        categories.forEach((category, documents) -> {
            documents.forEach((id, terms) -> terms.forEach((term, value) -> {
                tries[0]++;
                try {
                    weightsOfCategories.get(category).get(term);
                } catch (NullPointerException e) {
                    catched[0]++;
                }
            }));
        });
        assertThat(catched[0]).isEqualTo(tries[0]);
    }

    @Test
    public void constructorMustParseAndStore_AllTermsFrom_CategoriesHashMapWithFullyOverlappingTermsMaps() {
        documents.add("1",terms);
        documents.add("2",terms);
        categories.add(new Category("Cat"), documents);
        WeightsOfCategories weightsOfCategories = new WeightsOfCategories(categories);
        categories.forEach((category, documents) -> {
            documents.forEach((id, terms) -> terms.forEach((term, value) -> {
                assertThat(weightsOfCategories.get(category).get(term))
                        .isNotNull();
            }));
        });
    }
}