package kspt.team5.mailcat.WBClassifier.containers;

import kspt.team5.mailcat.core.entities.Category;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CategoriesHashMapTest {
    private String id = "7";
    private Category category = new Category("Категория");
    private String word = "Слово";
    private Double value = 10d;
    private CategoriesHashMap categories = new CategoriesHashMap();
    private DocumentsHashMap documents = new DocumentsHashMap();
    private TermsHashMap terms = new TermsHashMap();

    @Test
    public void test_BasicHashMapMethods() {
        terms.put(word, value);
        documents.put(id, terms);
        categories.put(category, documents);
        assertThat(categories.get(category).get(id).get(word)).isEqualTo(value);
    }

    @Test
    public void test_AddMethod_ActuallyWritesSum() {
        terms.put(word, value);
        documents.put(id, terms);
        categories.put(category, documents);
        categories.add(category, documents);
        assertThat(categories.get(category).get(id).get(word)).isLessThan   (20.0000000000001d);
        assertThat(categories.get(category).get(id).get(word)).isGreaterThan(19.999999999999d);
    }

    @Test
    public void addMethod_OnEmptyHashPair_ShoudActLikePutMethod() {
        terms.put(word, value);
        documents.put(id, terms);
        categories.add(category, documents);
        assertThat(categories.get(category).get(id).get(word)).isEqualTo(value);
    }

    @Test
    public void addMethod_OnExpandingTermHashMap_ShoulAddNewTerms() {
        terms.put(word, value);
        documents.put(id, terms);
        DocumentsHashMap expandingDocuments = new DocumentsHashMap();
        expandingDocuments.put(id, terms);
        expandingDocuments.put(id+1, terms);
        expandingDocuments.put(id+2, terms);
        categories.add(category, documents);
        categories.add(category, expandingDocuments);
        assertThat(categories.get(category).get(id).get(word)).isLessThan   (20.0000000000001d);
        assertThat(categories.get(category).get(id).get(word)).isGreaterThan(19.999999999999d);
        assertThat(categories.get(category).get(id+1).get(word)).isEqualTo(value);
        assertThat(categories.get(category).get(id+2).get(word)).isEqualTo(value);
    }
}
