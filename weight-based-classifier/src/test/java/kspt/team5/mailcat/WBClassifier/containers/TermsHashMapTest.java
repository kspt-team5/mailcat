package kspt.team5.mailcat.WBClassifier.containers;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TermsHashMapTest {
    private String word = "Слово";

    @Test
    public void test_BasicHashMapMethods(){
        TermsHashMap terms = new TermsHashMap();
        terms.put(word, 10d);
        assertThat(terms.get(word)).isEqualTo(10d);
    }

    @Test
    public void test_AddMethod(){
        TermsHashMap terms = new TermsHashMap();
        terms.put(word, 1000.1d);
        terms.add(word, 1.2d);
        assertThat(terms.get(word)).isLessThan    (1001.30000000001d);
        assertThat(terms.get(word)).isGreaterThan (1001.2999999999d);
    }

    @Test
    public void addMethod_OnEmptyHashPair_ShoudActLikePutMethod(){
        TermsHashMap terms = new TermsHashMap();
        terms.add(word, 1.2d);
        assertThat(terms.get(word)).isEqualTo(1.2d);
    }
}
