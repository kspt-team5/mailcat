package kspt.team5.mailcat.WBClassifier;

import kspt.team5.mailcat.WBClassifier.actors.WBMailClassifier;
import kspt.team5.mailcat.core.entities.Category;
import kspt.team5.mailcat.core.entities.Email;
import org.junit.Test;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class TestUncategorizedCoefficientFinder {
    // "Sorted" and "Inbox" folders must be in "out" folder. To be invisible for git.
    private Path startPath = Paths.get(System.getProperty("user.dir"));
    private Path categoriesPath = Paths.get(startPath.toString() + File.separator + "out" + File.separator + "Sorted");
    private Path inboxPath = Paths.get(startPath.toString() + File.separator + "out" + File.separator + "Inbox");
    private String fileRealParent;
    private double success;
    private double uncategorized;
    private double allCountableTries;
    private double allMessages;
    private double margin;

    //@Test
    public void findBestUncategorizedCoefficient() {
        margin = 0.2;
        //for (margin = 0; margin <= 1.5; margin+=0.1)
            testAccuracy();

        /*
         * Result should be like
         * Margin = 0.2 Success rate = 74.03846153846155%	Uncategorized rate = 22.962962962962962%
         *
         */
    }

    public void testAccuracy() {
        success = 0d;
        uncategorized = 0d;
        allCountableTries = 0d;
        allMessages = 0d;
        for (File category : new File(categoriesPath.toString()).listFiles())
            if (category.isDirectory())
                for (File message : category.listFiles())
                    if (message.isFile())
                        performTest(message);
        System.out.print("Margin = " + margin + " ");
        System.out.print("Success rate = " + (success / allCountableTries * 100d) + "%\t");
        System.out.println("Uncategorized rate = " + (uncategorized / allMessages * 100d) + "%");
    }

    private void performTest(File message) {
        pickMessage(message);
        runAndCheck(message);
        putMessageBack();
    }

    private void pickMessage(File message) {
        fileRealParent = message.getParentFile().getName();
        message.renameTo(new File(inboxPath.toString() + File.separator + message.getName()));
    }

    private void runAndCheck(File message) {
        Category result = runPrototype();
        try {
            if (result.getName().equals(fileRealParent)) {
                System.out.println("+ " + result.getName() + ": " + message.getName());
                success += 1d;
            } else {
                System.out.println("- " + result.getName() + " (" + fileRealParent + "): " + message.getName());
            }
            allCountableTries += 1d;
        } catch (NullPointerException e) {
            System.out.println("? " + "Uncategorized" + " (" + fileRealParent + "): " + message.getName());
            uncategorized += 1d;
        }
        allMessages += 1d;
    }

    private void putMessageBack() {
        for (File message : new File(inboxPath.toString()).listFiles())
            message.renameTo(new File(categoriesPath.toString() + File.separator + fileRealParent + File.separator + message.getName()));
    }

    private Category runPrototype() {
        WBMailClassifier prototype = new WBMailClassifier();
        Email incomingMsg = GetTestingResource.loadJsonEmail(new File(inboxPath.toString()).listFiles()[0], null);
        ArrayList<Email> emails = GetTestingResource.loadJsonCategorizedEmails(categoriesPath.toString());
        for (Email email : emails)
            prototype.teach(email, new Category(email.getRecipient()));
        prototype.setMargin(margin);
        return prototype.classify(incomingMsg);
    }
}

