package kspt.team5.mailcat.WBClassifier.actors;

import kspt.team5.mailcat.WBClassifier.GetTestingResource;
import kspt.team5.mailcat.WBClassifier.containers.TermsHashMap;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ParserTest {
    private String contentHtml = GetTestingResource.asString("content.txt");
    private String contentPlain = GetTestingResource.asString("content.txt");

    @Test
    public void parseTermsHtml() throws Exception {
        TermsHashMap terms = Parser.parseTermsHtml(contentHtml);
        assertThat(terms).isNotNull();
        terms.forEach(
                (term, value) -> {
                    assertThat(term).doesNotContainPattern("[^A-zА-я]");
                    assertThat(term).isNotEmpty();
                }
        );
    }

    @Test
    public void parseTermsPlain() throws Exception {
        TermsHashMap terms = Parser.parseTermsPlain(contentPlain);
        assertThat(terms).isNotNull();
        terms.forEach(
                (term, value) -> {
                    assertThat(term).doesNotContainPattern("[^A-zА-я]");
                    assertThat(term).isNotEmpty();
                }
        );
    }
}