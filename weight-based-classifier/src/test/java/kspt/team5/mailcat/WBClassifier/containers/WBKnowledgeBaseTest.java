package kspt.team5.mailcat.WBClassifier.containers;

import kspt.team5.mailcat.core.entities.Category;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class WBKnowledgeBaseTest {
    private Category category = new Category("Категория");
    private String id = "7";
    private String word = "Слово";
    private Double value = 10d;

    @Test
    public void testPityGetter() {
        CategoriesHashMap base = new CategoriesHashMap();
        TermsHashMap terms = new TermsHashMap();
        terms.put(word, value);
        DocumentsHashMap documents = new DocumentsHashMap();
        documents.put(id, terms);
        base.put(category, documents);
        assertThat(base.get(category).get(id).get(word))
                .isEqualTo(value);
    }
}
