package kspt.team5.mailcat.WBClassifier.actors;

import kspt.team5.mailcat.WBClassifier.GetTestingResource;
import kspt.team5.mailcat.core.entities.Category;
import kspt.team5.mailcat.core.entities.Email;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class WBMailClassifierTest {
    private String content1 = GetTestingResource.asString("content.txt");
    private String content2 = GetTestingResource.asString("baneks.txt");
    private Email email1 = new Email("777", "Тема письма", content1, content1, "b@b.b", "a@a.a");
    private Email email2 = new Email("778", "Тема письма", content1, content1, "b@b.b", "a@a.a");
    private Category category1 = new Category("Категория");
    private Category category2 = new Category("Анекдоты");
    private WBMailClassifier classifier = new WBMailClassifier();

    @Test
    public void classifyMethod_ReturnsUncategorized_IfKnowledgeBaseIsEmpty() {
        assertThat(classifier.classify(email1))
                .isNull();
    }

    @Test
    public void classifyMethod_ReturnsUncategorized_IfEmailContentIsEmpty() {
        classifier.teach(email1, category1);
        classifier.teach(email2, category1);
        Email emptyContentEmail = new Email("1", "", "", "", "", "");
        assertThat(classifier.classify(emptyContentEmail))
                .isNull();
    }

    @Test
    public void classifyMethod_ReturnsUncategorized_IfNoCategoriesContainsMoreThanOneDocument() {
        classifier.teach(email1, category1);
        classifier.teach(email2, category2);
        classifier.teach(email1, new Category("ящерица"));
        assertThat(classifier.classify(email1))
                .isNull();
    }

    @Test
    public void classifyMethod_ReturnsUncategorized_IfEmailContentHasOnlyStrippableContent() {
        classifier.teach(email1, category1);
        classifier.teach(email2, category1);
        Email incomingEmail = new Email("1", "", "the me are я Вы ты не 9, 18", "the me are я Вы ты не 9, 18", "", "");
        assertThat(classifier.classify(incomingEmail))
                .isNull();
    }

    @Test
    public void classifyMethod_ReturnsProperCategory_CausedByWittinglyInputData() {
        Email teachingEmail1 = new Email("777", "", content1, content1, "", "");
        Email teachingEmail2 = new Email("778", "", content1, content1, "", "");
        Email baneksCatEmail1 = new Email("1", "", content2, content2, "", "");
        Email baneksCatEmail2 = new Email("2", "", content2, content2, "", "");
        Email incomingEmail = new Email("3", "", content2, content2, "", "");
        classifier.teach(teachingEmail1, category1);
        classifier.teach(teachingEmail2, category1);
        classifier.teach(baneksCatEmail1, category2);
        classifier.teach(baneksCatEmail2, category2);
        assertThat(classifier.classify(incomingEmail))
                .isEqualTo(category2);
    }

    @Test
    public void teachMethod_CreatesCategory_InKnowledgeBase() {
        classifier.teach(email1, category1);
        assertThat(classifier.getKnowledgeBase().get(category1))
                .isNotNull();
    }

    @Test
    public void teachMethod_PutsStrippedAndStemmedTerms_ToKnowledgeBase() {
        classifier.teach(email1, category1);
        assertThat(classifier.getKnowledgeBase().get(category1)).isNotNull();
        classifier.getKnowledgeBase().get(category1).get(email1.getId()).forEach(
                (term, value) -> {
                    assertThat(term).doesNotContainPattern("[^A-zА-я]");
                    assertThat(term).isNotEmpty();
                }
        );
    }
}
